package com.totalplay.modulepayandroid;

import android.os.Bundle;
import android.view.MenuItem;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.resources.mvp.BaseActivity;
import com.resources.mvp.BasePresenter;
import com.totalplay.modulepayandroid.Utils.CustomTitleBar;

public class MainAppCompatActivity extends BaseActivity {
    public static AppCompatActivity mAppCompatActivity;
    private BasePresenter[] mPresenters;
    private BasePresenter mPresenter;
    private boolean toolbarEnabled;
    private boolean homeAsUpEnabled;

    public FirebaseAnalytics mFirebaseAnalytics;
    public FirebaseCrashlytics firebaseCrashlytics;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        toolbarEnabled = true;
        homeAsUpEnabled = true;
        mAppCompatActivity = this;

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        firebaseCrashlytics = FirebaseCrashlytics.getInstance();


        mPresenters = getPresenters();
        mPresenter = getPresenter();
        if (mPresenters != null) {
            for (BasePresenter basePresenter : mPresenters) {
                if (basePresenter != null) {
                    basePresenter.onCreate();
                }
            }
        }
        if (mPresenter != null) mPresenter.onCreate();
    }

    protected BasePresenter getPresenter() {
        return null;
    }

    protected BasePresenter[] getPresenters() {
        return null;
    }

    public void setSubtitle(String subtitle) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setSubtitle(subtitle);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mPresenters != null) {
            for (BasePresenter basePresenter : mPresenters) {
                if (basePresenter != null) {
                    basePresenter.onStart();
                }
            }
        }
        if (mPresenter != null) mPresenter.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mPresenters != null) {
            for (BasePresenter basePresenter : mPresenters) {
                if (basePresenter != null) {
                    basePresenter.onResume();
                }
            }
        }
        if (mPresenter != null) mPresenter.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mPresenters != null) {
            for (BasePresenter basePresenter : mPresenters) {
                if (basePresenter != null) {
                    basePresenter.onPause();
                }
            }
        }
        if (mPresenter != null) mPresenter.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mPresenters != null) {
            for (BasePresenter basePresenter : mPresenters) {
                if (basePresenter != null) {
                    basePresenter.onStop();
                }
            }
        }
        if (mPresenter != null) mPresenter.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mPresenters != null) {
            for (BasePresenter basePresenter : mPresenters) {
                if (basePresenter != null) {
                    basePresenter.onDestroy();
                }
            }
        }
        if (mPresenter != null) mPresenter.onDestroy();
    }

    private void setupToolbar() {
        if (getSupportActionBar() != null && homeAsUpEnabled)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void onSetActivity(AppCompatActivity mAppCompatActivity){
        this.mAppCompatActivity = mAppCompatActivity;
    }

    public void onSetTitle(String mTitle){
        CustomTitleBar.mTextView.setText(mTitle);
    }

}
