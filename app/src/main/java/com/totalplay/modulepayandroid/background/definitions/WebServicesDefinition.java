package com.totalplay.modulepayandroid.background.definitions;


import com.totalplay.modulepayandroid.background.models.Request.Pay.PayRequest;
import com.totalplay.modulepayandroid.modules.divideExpense.models.request.PayersRequest;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface WebServicesDefinition {

    //balance user
    @GET("/fintech/balance2")
    Call<ResponseBody> getCustomerBalance(@Query("uid") String uid);

    //history
    @GET("/fintech/history2")
    Call<ResponseBody> getHistoryOrders(@Query("uid") String uid ,@Query("offset") int offset );

    //pget data pagador
    @GET("/fintech/getPayee")
    Call<ResponseBody> getDataPay(@Query("uid") String uid);

    @POST("/fintech/payment2")
    Call<ResponseBody> createPay(@Body PayRequest payRequest);

    //solicitud de pago al dividir cuenta
    @POST("/fintech/crearSolicitud")
    Call<ResponseBody> sendRequestPayers(@Body PayersRequest payersRequest);


}
