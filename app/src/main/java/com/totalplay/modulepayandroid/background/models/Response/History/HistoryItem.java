package com.totalplay.modulepayandroid.background.models.Response.History;

import com.google.gson.annotations.SerializedName;

public class HistoryItem{

	@SerializedName("date")
	private String date;

	@SerializedName("reference")
	private String reference;

	@SerializedName("amount")
	private String amount;

	@SerializedName("type")
	private String type;

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	public void setReference(String reference){
		this.reference = reference;
	}

	public String getReference(){
		return reference;
	}

	public void setAmount(String amount){
		this.amount = amount;
	}

	public String getAmount(){
		return amount;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}
}