package com.totalplay.modulepayandroid.background;

import android.annotation.SuppressLint;

import com.ihsanbal.logging.Level;
import com.ihsanbal.logging.LoggingInterceptor;
import com.totalplay.modulepayandroid.BuildConfig;
import com.totalplay.modulepayandroid.background.definitions.WebServicesDefinition;
import com.totalplay.modulepayandroid.common.model.pojo.User;
import com.totalplay.modulepayandroid.data.PreferenceHelper;

import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.internal.platform.Platform;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class webServices {

    private static WebServicesDefinition servicesDefinition;
    private static WebServicesDefinition servicesDefinitionAuth;
    public static final String HTK = "htk";

    public static WebServicesDefinition services() {
        if (servicesDefinition == null) {
            setupServicesDefinition();
        }
        return servicesDefinition;
    }

    public static WebServicesDefinition servicesAuth() {
        if (servicesDefinitionAuth == null) {
            setupServicesAuthDefinition();
        }
        return servicesDefinitionAuth;

    }

    private static void setupServicesDefinition() {
        OkHttpClient.Builder builder = getBuilder(false);
        servicesDefinition = getRetrofitClient(builder).create(WebServicesDefinition.class);
    }

    private static void setupServicesAuthDefinition() {
        OkHttpClient.Builder builder = getBuilder(true);
        servicesDefinitionAuth = getRetrofitClient(builder).create(WebServicesDefinition.class);
    }

    private static Retrofit getRetrofitClient(OkHttpClient.Builder builder) {
        return new Retrofit.Builder()
                .baseUrl("https://totalplay-dev.apigee.net/")
                .client(getUnsafeOkHttpClient(builder))
                .addConverterFactory(GsonConverterFactory.create(GsonTools.gsonForDeserialization()))
                .build();
    }

    private static OkHttpClient.Builder getBuilder(boolean auth) {
        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.readTimeout(120, TimeUnit.SECONDS);
        builder.connectTimeout(120, TimeUnit.SECONDS);

        builder.addInterceptor(chain -> {
            Request.Builder builder1 = chain.request().newBuilder();
            builder1.headers(getJsonHeader(auth));
            return chain.proceed(builder1.build());
        });

        return builder;
    }


    private static Headers getJsonHeader(boolean auth) {
        Headers.Builder builder = new Headers.Builder();
        builder.add("Content-Type", "application/json");
        builder.add("Accept", "application/json");
        if(auth){
            builder.add("Authorization", "Bearer " + PreferenceHelper.getString(User.TKA));
        }
        return builder.build();
    }

    @SuppressLint("TrustAllX509TrustManager")
    private static OkHttpClient getUnsafeOkHttpClient(OkHttpClient.Builder builder) {
        try {

            builder.hostnameVerifier((hostname, session) -> {
                if (!BuildConfig.DEBUG) {
                    HostnameVerifier hostnameVerifier = HttpsURLConnection.getDefaultHostnameVerifier();
                    return hostnameVerifier.verify(hostname, session);
                }
                return true;
            });

            builder.addInterceptor(new LoggingInterceptor.Builder()
                    .loggable(BuildConfig.DEBUG)
                    .setLevel(Level.BASIC)
                    .log(Platform.INFO)
                    .request("Request")
                    .response("Response")
                    .addHeader("version", BuildConfig.VERSION_NAME)
                    .build());

            return builder.build();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
