package com.totalplay.modulepayandroid.background.models.Request.Pay;

import com.google.gson.annotations.SerializedName;

public class PayRequest{

	@SerializedName("payee")
	private String payee;

	@SerializedName("reference")
	private String reference;

	@SerializedName("amount")
	private float amount;

	@SerializedName("factura")
	private boolean factura;

	@SerializedName("tip")
	private float tip;

	@SerializedName("payer")
	private String payer;

	@SerializedName("order")
	private String order;

	public void setPayee(String payee){
		this.payee = payee;
	}

	public String getPayee(){
		return payee;
	}

	public void setReference(String reference){
		this.reference = reference;
	}

	public String getReference(){
		return reference;
	}

	public void setAmount(float amount){
		this.amount = amount;
	}

	public float getAmount(){
		return amount;
	}

	public void setFactura(boolean factura){
		this.factura = factura;
	}

	public boolean isFactura(){
		return factura;
	}

	public void setTip(float tip){
		this.tip = tip;
	}

	public float getTip(){
		return tip;
	}

	public void setPayer(String payer){
		this.payer = payer;
	}

	public String getPayer(){
		return payer;
	}

	public void setOrder(String order){
		this.order = order;
	}

	public String getOrder(){
		return order;
	}
}