package com.totalplay.modulepayandroid.background;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GsonTools {
    public static Gson gsonForDeserialization() {
        return new GsonBuilder()
                .create();
    }
}
