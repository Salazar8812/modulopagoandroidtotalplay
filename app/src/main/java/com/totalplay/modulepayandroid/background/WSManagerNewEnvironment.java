package com.totalplay.modulepayandroid.background;

import com.resources.background.BaseDefinition;
import com.resources.background.BaseWSManager;

public class WSManagerNewEnvironment extends BaseWSManager {

    public static WSManagerNewEnvironment init() {
        return new WSManagerNewEnvironment();
    }

    public static String getJsonDebugUnit(String requestUrl) {
        String json = "";
        switch(requestUrl){
            case WSManagerNewEnvironment.WS.REQUEST_PAYERS:
                json = "{\n" +
                        "    \"message\": \"Se envió la solicitud de cobro a los destinatarios.\"\n" +
                        "}";
                break;
        }
        return json;
    }

    @Override
    protected boolean getDebugEnabled() {
        return false;
    }

    @Override
    protected boolean getErrorDebugEnabled() {
        return false;
    }

    public static class WS {
        public static final String CREATEPAYORDERP2P = "wsCreatePayOrderP2P";
        public static final String HISTORY_ORDERS = "wsHistoryOrders";
        public static final String BALANCE = "wsBalance";
        public static final String DATA_PAY = "wsDataPay";
        public static final String BALANCE_COMMERCE = "wsBalanceCommerce";
        public static final String REQUEST_PAYERS = "wsRequestPayers";
    }

    @Override
    protected BaseDefinition getDefinition() {
        return null;
    }

    @Override
    protected String getJsonDebug(String requestUrl) {
        return null;
    }

}

