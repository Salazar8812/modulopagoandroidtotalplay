package com.totalplay.modulepayandroid.background.models.Response.Pay;

import com.google.gson.annotations.SerializedName;
import com.resources.background.WSBaseResponse;

public class DataPayResponse extends WSBaseResponse {

	@SerializedName("message")
	private String message;

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}
}