package com.totalplay.modulepayandroid.background.models.Response.DataQR;

import com.google.gson.annotations.SerializedName;
import com.resources.background.WSBaseResponse;

public class PayeeResponse extends WSBaseResponse {

	@SerializedName("tipo")
	private String tipo;

	@SerializedName("emiteFactura")
	private boolean emiteFactura;

	@SerializedName("categoria")
	private String categoria;

	@SerializedName("aceptaPropina")
	private boolean aceptaPropina;

	@SerializedName("nombre")
	private String nombre;

	public void setTipo(String tipo){
		this.tipo = tipo;
	}

	public String getTipo(){
		return tipo;
	}

	public void setEmiteFactura(boolean emiteFactura){
		this.emiteFactura = emiteFactura;
	}

	public boolean isEmiteFactura(){
		return emiteFactura;
	}

	public void setCategoria(String categoria){
		this.categoria = categoria;
	}

	public String getCategoria(){
		return categoria;
	}

	public void setAceptaPropina(boolean aceptaPropina){
		this.aceptaPropina = aceptaPropina;
	}

	public boolean isAceptaPropina(){
		return aceptaPropina;
	}

	public void setNombre(String nombre){
		this.nombre = nombre;
	}

	public String getNombre(){
		return nombre;
	}
}