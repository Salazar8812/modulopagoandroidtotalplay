package com.totalplay.modulepayandroid.background.models.Response.balance;

import com.google.gson.annotations.SerializedName;
import com.resources.background.WSBaseResponse;

public class BalanceResponse extends WSBaseResponse {

	@SerializedName("balance")
	private String balance;

	public void setBalance(String balance){
		this.balance = balance;
	}

	public String getBalance(){
		return balance;
	}
}