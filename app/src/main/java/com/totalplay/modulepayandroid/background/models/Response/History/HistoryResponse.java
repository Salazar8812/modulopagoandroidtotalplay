package com.totalplay.modulepayandroid.background.models.Response.History;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.resources.background.WSBaseResponse;

public class HistoryResponse extends WSBaseResponse {

	@SerializedName("history")
	private List<HistoryItem> history;

	public void setHistory(List<HistoryItem> history){
		this.history = history;
	}

	public List<HistoryItem> getHistory(){
		return history;
	}

}