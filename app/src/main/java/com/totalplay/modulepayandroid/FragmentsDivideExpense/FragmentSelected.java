package com.totalplay.modulepayandroid.FragmentsDivideExpense;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.totalplay.modulepayandroid.modules.divideExpense.models.Contact;
import com.totalplay.modulepayandroid.modules.divideExpense.view.adapters.ContactsAdapter;
import com.totalplay.modulepayandroid.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentSelected extends Fragment implements ContactsAdapter.onItemClickListener {
        @BindView(R.id.mRecyclerAll)
        RecyclerView mRecyclerAll;

        ContactsAdapter mContactsAdapter;

        @Nullable
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View v = inflater.inflate(R.layout.fragment_all, container, false);
            ButterKnife.bind(this,v);
            createAdapter();
            return v;
        }

        void createAdapter(){
            mRecyclerAll.setLayoutManager(new LinearLayoutManager(getActivity()));
            mContactsAdapter = new ContactsAdapter(getListContacts(), this);
            mRecyclerAll.setAdapter(mContactsAdapter);
        }

    private ArrayList<Contact> getListContacts() {
        ArrayList<Contact> contacts = new ArrayList<>();
        contacts.add(new Contact("luis", "1", true, false));
        contacts.add(new Contact("Gerardo", "2", false, false));
        contacts.add(new Contact("Jose", "3", true, false));
        contacts.add(new Contact("Maria", "4", false, false));
        return contacts;
    }


    @Override
    public void onItemApplyClick(View v, boolean b) {

    }

    @Override
    public void onItemFavoriteClick(View v, boolean b) {

    }
}
