package com.totalplay.modulepayandroid.data;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceHelper{

    private Context context;
    private static  SharedPreferences preference;
    private static final String MIS_PREFERENCES = "mis_preferencias";
    private static final String DEFAULT = "";

    public PreferenceHelper() {
    }

    public static void init(Context context){
        if(preference == null){
            preference = context.getSharedPreferences(context.getPackageName(), Activity.MODE_PRIVATE);
        }
    }

    public static void saveString(String key, String value){
        if(preference != null){
            SharedPreferences.Editor editor = preference.edit();
            editor.putString(key, value);
            editor.apply();
        }
    }

    public static String getString(String key){
        return preference.getString(key, DEFAULT);
    }

    public static void saveBoolean(String key, boolean value){
        if(preference != null){
            SharedPreferences.Editor editor = preference.edit();
            editor.putBoolean(key, value);
            editor.apply();
        }
    }

    public static boolean getBoolean(String key){
        return preference.getBoolean(key, false);
    }

    public static void clearData(){
        preference.edit().clear().apply();
    }




}
