package com.totalplay.modulepayandroid.modules.notifications.view.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.totalplay.modulepayandroid.R;
import com.totalplay.modulepayandroid.modules.notifications.model.NotificationLocal;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class NotificationsNoVistasAdapter extends RecyclerView.Adapter<NotificationsNoVistasAdapter.ViewHolder> {

    private ArrayList<NotificationLocal> mNotifications;
    private onItemClickListener listener;

    public NotificationsNoVistasAdapter(ArrayList<NotificationLocal> notifications, onItemClickListener listener) {
        this.mNotifications = notifications;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification, parent, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final NotificationLocal notification = mNotifications.get(position);
        holder.tvNamePersonNotification.setText(notification.getPayee());
        holder.tvDateNotification.setText(notification.getDate());
        holder.tvAmountNotification.setText("$" + notification.getMonto());
        holder.itemView.setTag(position);
        holder.itemView.setOnClickListener(view -> {
            listener.onItemClick(holder.itemView);
        });
    }

    @Override
    public int getItemCount() {
        return mNotifications.size();
    }

    public void update(ArrayList<NotificationLocal> notificationLocals) {
        this.mNotifications.clear();
        this.mNotifications.addAll(notificationLocals);
        this.notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_name_person_notification)
        TextView tvNamePersonNotification;
        @BindView(R.id.tv_date_notification)
        TextView tvDateNotification;
        @BindView(R.id.tv_amount_notification)
        TextView tvAmountNotification;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface onItemClickListener {

        void onItemClick(View v);
    }

}

