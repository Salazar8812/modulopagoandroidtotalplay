package com.totalplay.modulepayandroid.modules.notifications.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.resources.utils.MessageUtils;
import com.totalplay.modulepayandroid.MainAppCompatActivity;
import com.totalplay.modulepayandroid.R;
import com.totalplay.modulepayandroid.Utils.BiometricView;
import com.totalplay.modulepayandroid.common.view.dialogs.ConfirmPasswordDialog;
import com.totalplay.modulepayandroid.firebase.BiometricPresenter;
import com.totalplay.modulepayandroid.modules.notifications.presenters.PayPayeePresenter;
import com.totalplay.modulepayandroid.modules.notifications.presenters.UpdateNotificationPresenter;
import com.totalplay.modulepayandroid.modules.pay.view.activities.PayActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PaymentRequestActivity extends MainAppCompatActivity implements BiometricView, PayPayeePresenter.View {
    public static final String DATE = "date";
    public static final String FRIEND = "mFriend";

    public static final String QUANTITY = "mQuantity";
    public static final String REFENRENCE = "mFriend";
    public static final String ORDER = "mOrder";
    public static final String ID_PAYEER = "m_id_payeer"; //el que paga
    public static final String ID_PAYEE = "m_id_payee"; //al que le pagan
    public static final String ID_NOTIFY = "id_notify";
    public static final String SEE = "see";


    private String mPayee = "";
    private String mDate = "";

    private String mMonto = "";
    private String mRef = "";
    private String mOrder = "";
    private String mIdPayeer = "";
    private String mIdPayee = "";
    private String mIdNotify = "";
    private boolean mSee = false;

    @BindView(R.id.btn_pay_payment_request)
    Button btnLoginStartSession;
    @BindView(R.id.tv_exit_payment_request)
    TextView tvExitPaymentRequest;
    @BindView(R.id.progresss)
    ConstraintLayout progresss;

    @BindView(R.id.mTvQuantity)
    TextView mTvQuantity;
    @BindView(R.id.mTvRequest)
    TextView mTvRequest;
    @BindView(R.id.mTvAcceptAndPay)
    TextView mTvAcceptAndPay;

    private BiometricPresenter biometricPresenter;
    private PayPayeePresenter mCreatePayOrderPresenter;
    private UpdateNotificationPresenter updateNotificationPresenter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.activity_payment_request);
        ButterKnife.bind(this);
        initPresenter();
        initProgress();
        getIntents();
        checkSaw();
        mTvQuantity.setText(String.format("$%s", mMonto));
        mTvRequest.setText(String.format("%s %s ", mPayee, getText(R.string.te_envio)));
        mTvAcceptAndPay.setText(String.format("%s%s?", getText(R.string.aceptar_y_pagar_a), getIntent().getStringExtra(FRIEND)));
    }

    private void initProgress() {
        enabledElements(true);
        progress(false);
    }

    private void enabledElements(boolean b) {
        btnLoginStartSession.setEnabled(b);
        tvExitPaymentRequest.setEnabled(b);
    }

    private void progress(boolean b) {
        progresss.setVisibility(b ? View.VISIBLE : View.GONE);
    }

    private void checkSaw() {
        if (!mSee) {
            updateNotify();
        }
    }

    private void initPresenter() {
        biometricPresenter = new BiometricPresenter(this, this);
        mCreatePayOrderPresenter = new PayPayeePresenter(this, this);
        updateNotificationPresenter = new UpdateNotificationPresenter(this);
    }

    private void getIntents() {
        mPayee = getIntent().getStringExtra(FRIEND);
        mDate = getIntent().getStringExtra(DATE);
        mMonto = getIntent().getStringExtra(QUANTITY);
        mRef = getIntent().getStringExtra(REFENRENCE);
        mOrder = getIntent().getStringExtra(ORDER);
        mIdPayeer = getIntent().getStringExtra(ID_PAYEER);
        mIdPayee = getIntent().getStringExtra(ID_PAYEE);
        mIdNotify = getIntent().getStringExtra(ID_NOTIFY);
        mSee = getIntent().getBooleanExtra(SEE, false);
    }

    private void updateNotify() {
        updateNotificationPresenter.updateViewNotifiy(mIdNotify, new UpdateNotificationPresenter.UpdateNotifyView() {
            @Override
            public void onSucess() {
            }

            @Override
            public void onError() {
                //TOD analitics or crashlitycs add

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        onSetActivity(this);
        onSetTitle("SOLICITUD DE COBRO");
    }

    @OnClick(R.id.btn_pay_payment_request)
    public void onClick() {
        biometricPresenter.validationBiometricExist();
    }

    private void startNewActivity() {
        Intent mIntent1 = new Intent(mAppCompatActivity, PayPayeeActivity.class);
        mIntent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        mIntent1.putExtra(PayPayeeActivity.QUANTITY, mMonto);
        mIntent1.putExtra(PayPayeeActivity.FRIEND, mPayee);
        mIntent1.putExtra(PayPayeeActivity.DATE, mDate);
        mIntent1.putExtra(PayPayeeActivity.REFERENCE, mRef);
        mIntent1.putExtra(PayPayeeActivity.ORDER, mOrder);
        startActivity(mIntent1);

    }

    @Override
    public void onBiometricEnabled() {
        biometricPresenter.getBiometricPrompt().authenticate(biometricPresenter.getPrompInfo());
    }

    @Override
    public void onBiometricDisabled(String msg) {
        //selectTypeTransder(mIdPayee);
        ConfirmPasswordDialog confirmPasswordDialog = ConfirmPasswordDialog.newInstance();
        confirmPasswordDialog.getAuthCredentials(new PayActivity.payCredentials() {

            @Override
            public void onSuccess() {
                transfer();
            }

            @Override
            public void onFailure(String resMsg) {
                enabledElements(true);
                progress(false);
                MessageUtils.toast(getApplicationContext(), "Error de usuario o contraseña");
            }
        });
        confirmPasswordDialog.show(this.getSupportFragmentManager(), "confirmPassword");
        //startActivityForResult(new Intent(this, DialogValidateSession.class), 666);

    }

    private void transfer() {
        enabledElements(false);
        progress(true);
        mCreatePayOrderPresenter.CreatePayOrdersP2P(mMonto, mIdPayee, mOrder, mRef);
    }

    @Override
    public void onAuthenticationError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAuthenticationSucceeded() {
        transfer();
    }

    @Override
    public void onAuthenticationFailed(String msg) {
        enabledElements(true);
        progress(false);
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    /*interface payPayeePresenter*/

    @Override
    public void onSucessPay(String monto, String mPayee, String mReference, String mOrder) {
        enabledElements(true);
        progress(false);
        startNewActivity();
    }

    @Override
    public void onErrorPay(String msg) {
        enabledElements(true);
        progress(false);
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();

    }
}
