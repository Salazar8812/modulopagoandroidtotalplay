package com.totalplay.modulepayandroid.modules.divideExpense.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.totalplay.modulepayandroid.MainAppCompatActivity;
import com.totalplay.modulepayandroid.R;
import com.totalplay.modulepayandroid.modules.main.view.activities.MyAccountNewActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SuccessfulPaymentRequestActivity extends MainAppCompatActivity {
    @BindView(R.id.mTvReference)
    TextView mTvReference;

    public static final String NO_REFERENCE = "noReference";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.activity_successful_payment_request);
        ButterKnife.bind(this);
        mTvReference.setText(String.format("%s%s", getText(R.string.referencia_), getIntent().getStringExtra(NO_REFERENCE)));
    }

    @Override
    protected void onResume() {
        super.onResume();
        onSetActivity(this);
        onSetTitle("PAGO EXITOSO");
    }

    @OnClick({R.id.btn_login_start_session, R.id.tv_salir})
    public void onClick(View view) {
        Intent newIntent = new Intent(this, MyAccountNewActivity.class);
        newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        switch (view.getId()) {
            case R.id.btn_login_start_session:
                startActivity(newIntent);
                break;
            case R.id.tv_salir:
                startActivity(newIntent);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Intent newIntent = new Intent(this, MyAccountNewActivity.class);
        newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        super.onBackPressed();

    }
}
