package com.totalplay.modulepayandroid.modules.divideExpense.view.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.totalplay.modulepayandroid.R;
import com.totalplay.modulepayandroid.modules.divideExpense.models.Contact;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ViewHolder> implements Filterable {

    private ArrayList<Contact> mContacts;
    private ArrayList<Contact> contactsFull;
    private ArrayList<Contact> contactsRespaldo;
    private ArrayList<Contact> contactsFilter;
    private onItemClickListener mListener;

    public ContactsAdapter(ArrayList<Contact> mContacts, onItemClickListener mListener) {
        this.mContacts = mContacts;
        this.contactsFull = mContacts;
        this.contactsRespaldo = mContacts;
        this.mListener = mListener;
    }

    public void update(ArrayList<Contact> contacts) {
        this.mContacts = contacts;
        this.notifyDataSetChanged();
    }

    public ArrayList<Contact> getContactsFilter() {
        return mContacts;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_divide_expense, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Contact contact = mContacts.get(position);
        holder.tvNameContactDivideExpense.setText(contact.getName());

        holder.mApplyImageView.setVisibility(contact.isSelectPay() ? View.VISIBLE : View.GONE);
        holder.mStarImageView.setVisibility(contact.isFavorite() ? View.VISIBLE : View.GONE);

        holder.mApplyImageView2.setVisibility(contact.isSelectPay() ? View.GONE : View.VISIBLE);

        holder.itemView.setTag(position);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onItemApplyClick(holder.itemView, !contact.isSelectPay());
            }
        });

    }

    @Override
    public int getItemCount() {
        return mContacts.size();
    }

    @Override
    public Filter getFilter() {
        return exampleFilter;
    }

    private Filter exampleFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            ArrayList<Contact> filteredList = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(contactsFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                for (Contact item : contactsFull) {
                    if (item.getName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            contactsRespaldo = (ArrayList<Contact>) mContacts.clone();
            mContacts = (ArrayList<Contact>) results.values;
            notifyDataSetChanged();
        }
    };

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_name_contact_divide_expense)
        TextView tvNameContactDivideExpense;
        @BindView(R.id.mApplyImageView)
        ImageView mApplyImageView;
        @BindView(R.id.mStarImageView)
        ImageView mStarImageView;


        @BindView(R.id.mApplyImageView2)
        ImageView mApplyImageView2;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface onItemClickListener {
        void onItemApplyClick(View v, boolean b);

        void onItemFavoriteClick(View v, boolean b);
    }

}



