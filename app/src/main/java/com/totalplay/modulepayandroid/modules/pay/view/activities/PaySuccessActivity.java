package com.totalplay.modulepayandroid.modules.pay.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.totalplay.modulepayandroid.MainAppCompatActivity;
import com.totalplay.modulepayandroid.R;
import com.totalplay.modulepayandroid.Utils.OperationsUtils;
import com.totalplay.modulepayandroid.common.model.pojo.User;
import com.totalplay.modulepayandroid.data.PreferenceHelper;
import com.totalplay.modulepayandroid.modules.divideExpense.view.activities.DivideExpenseActivity;
import com.totalplay.modulepayandroid.modules.main.view.activities.MyAccountNewActivity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PaySuccessActivity extends MainAppCompatActivity {


    public static final String MONTO = "Monto";
    public static final String PAYEE = "mPayee";
    public static final String TIP = "mPropina";
    public static final String ORDER = "mOrder";
    public static final String REFERENCE = "mReference";
    public static final String TYPE_TRANSACTION = "type_transaction";
    public static final String USER_PAY = "user_pay";

    private static final String FLINK = "flink";
    private static final String IPTV = "iptv";
    private static final String USER = "users";

    private String typeTransaction;
    String mIdPayee;
    String mMonto;
    String mPropina;
    String mOrder;
    String mReference;
    String mUser;


    @BindView(R.id.mAmmountTextView)
    TextView mAmmountTextView;

    @BindView(R.id.mAccountIdTextView)
    TextView mAccountIdTextView;

    @BindView(R.id.mNameToTransferTextView)
    TextView mNameToTransferTextView;

    @BindView(R.id.mFechaTransferencia)
    TextView mFechaTransferencia;

    @BindView(R.id.mAmmountTextViewPropinaValue)
    TextView mAmmountTextViewPropinaValue;

    @BindView(R.id.mAmmountTextViewPropina)
    TextView mAmmountTextViewPropina;

    @BindView(R.id.tv_reference_pay_success)
    TextView tvReference;

    @BindView(R.id.tv_dividir_cuenta_pay_success)
    TextView tvDividirAcount;

    @BindView(R.id.sv_content_pay_success)
    ScrollView svContentPaySuccess;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.activity_pay_success_2);
        ButterKnife.bind(this);
        getIntents();
        checkDivideExpense();
        checkTip();
        mAccountIdTextView.setText(PreferenceHelper.getString(User.UID));
        //nameAccountSelect();
        setDateTransfer();
    }

    private void checkDivideExpense() {
        if (typeTransaction.equals(USER)) {
            tvDividirAcount.setVisibility(View.GONE);
        }
        tvDividirAcount.setVisibility(View.VISIBLE);
    }

    private void checkTip() {
        mNameToTransferTextView.setText(mUser);
        if (getIntent().getStringExtra(TIP) != null) {
            mAmmountTextViewPropina.setVisibility(View.VISIBLE);
            mAmmountTextViewPropinaValue.setVisibility(View.VISIBLE);
            mAmmountTextViewPropinaValue.setText(getTextCompose(getIntent().getStringExtra(TIP)));
            mOrder = getIntent().getStringExtra(ORDER);
            tvReference.setText("Referencia " + mReference + " Folio:" + mOrder);
        } else {
            mAmmountTextViewPropina.setVisibility(View.GONE);
            mAmmountTextViewPropinaValue.setVisibility(View.GONE);
        }
    }

    private void getIntents() {
        typeTransaction = getIntent().getStringExtra(TYPE_TRANSACTION);
        mAmmountTextView.setText(getTextCompose(getIntent().getStringExtra(MONTO)));
        mIdPayee = getIntent().getStringExtra(PAYEE);
        mMonto = getIntent().getStringExtra(MONTO);
        mPropina = getIntent().getStringExtra(TIP);
        mReference = getIntent().getStringExtra(REFERENCE);
        mUser = getIntent().getStringExtra(USER_PAY);
    }


    private String getTextCompose(String monto) {
        return "$" + OperationsUtils.getCurrentBalance(monto);
    }

    void setDateTransfer() {
        DateFormat df = new SimpleDateFormat("EEE, d MMM yyyy, HH:mm");
        String date = df.format(Calendar.getInstance().getTime());
        mFechaTransferencia.setText(date);
    }

    @Override
    protected void onResume() {
        super.onResume();
        onSetActivity(this);
        onSetTitle("PAGO EXITOSO");
    }

    @Override
    public void onSetActivity(AppCompatActivity mAppCompatActivity) {
        super.onSetActivity(this);
    }

    @OnClick({R.id.mButtonSalir, R.id.mButtonCompartir, R.id.tv_dividir_cuenta_pay_success})
    void OnClik(View v) {
        switch (v.getId()) {
            case R.id.mButtonSalir:
                startActivity(new Intent(this, MyAccountNewActivity.class));
                break;
            case R.id.mButtonCompartir:
                shareQR();
                break;
            case R.id.tv_dividir_cuenta_pay_success:
                Intent intent = new Intent(this, DivideExpenseActivity.class);
                intent.putExtra(DivideExpenseActivity.MONTO, getTotal(mMonto, mPropina));
                intent.putExtra(DivideExpenseActivity.REFERENCIA, mReference);
                intent.putExtra(DivideExpenseActivity.ORDER, mOrder);
                startActivity(intent);
                break;
            default:
                break;
        }
    }

    private float getTotal(String monto, String propina) {
        if (propina == null) {
            propina = "0";
        }
        return Float.parseFloat(monto) + Float.parseFloat(propina);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent newIntent = new Intent(this, MyAccountNewActivity.class);
        newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(newIntent);
    }

    void shareQR() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, "Transferencia TotalPay");
        intent.putExtra(Intent.EXTRA_TEXT, PreferenceHelper.getString(User.UID) + " te ha tranferido $" + mMonto + ".00 desde su cuenta TotalPay");
        startActivity(Intent.createChooser(intent, "Selecciona:"));
    }
}
