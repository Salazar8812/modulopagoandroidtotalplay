package com.totalplay.modulepayandroid.modules.main.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.resources.mvp.BasePresenter;
import com.totalplay.modulepayandroid.MainAppCompatActivity;
import com.totalplay.modulepayandroid.R;
import com.totalplay.modulepayandroid.common.model.pojo.User;
import com.totalplay.modulepayandroid.common.view.dialogs.CloseSessionDialog;
import com.totalplay.modulepayandroid.data.PreferenceHelper;
import com.totalplay.modulepayandroid.modules.divideExpense.view.adapters.BalanceAdapter;
import com.totalplay.modulepayandroid.modules.main.models.Order;
import com.totalplay.modulepayandroid.modules.main.presenters.BalancePresenter;
import com.totalplay.modulepayandroid.modules.main.presenters.HistoryPresenter;
import com.totalplay.modulepayandroid.modules.main.presenters.NumberNotifyPresenter;
import com.totalplay.modulepayandroid.modules.main.view.adapters.OrdersAdapter;
import com.totalplay.modulepayandroid.modules.notifications.view.activities.NotificationsActivity;
import com.totalplay.modulepayandroid.modules.pay.view.activities.OrderResumeActivity;
import com.totalplay.modulepayandroid.modules.pay.view.activities.SplitPaymentQr;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class MyAccountNewActivity extends MainAppCompatActivity implements HistoryPresenter.HistoryCallback, BalancePresenter.BalanceCallback {

    @BindView(R.id.mViewPager)
    ViewPager mViewPager;
    @BindView(R.id.mRecentMovementsRecyclerView)
    RecyclerView mRecentMovementsRecyclerView;

    @BindView(R.id.mTvTitleBottomSheet)
    TextView mTvTitleBottomSheet;
    @BindView(R.id.bottomSheet)
    LinearLayout mBottomSheet;
    @BindView(R.id.mTvName)
    TextView mTvName;

    @BindView(R.id.civ_my_acount_new)
    CircleImageView civMyAcountNew;
    @BindView(R.id.tv_number_notify)
    TextView tvNumberNotify;

    private OrdersAdapter ordersAdapter;
    private HistoryPresenter historyOrdersPresenter;
    private BalancePresenter customerBalancePresenter;
    private NumberNotifyPresenter numberNotifyPresenter;
    float stateBottomSheet = 0.0f;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.activity_my_account_new);
        ButterKnife.bind(this);
        initAdapter();
        setData();
        bottomSheet();
        historyOrdersPresenter.getHistory(PreferenceHelper.getString(User.UID));
        customerBalancePresenter.getBalance(PreferenceHelper.getString(User.UID));

    }

    @Override
    protected void onResume() {
        super.onResume();
        numberNotificationPresenter();
    }

    private void numberNotificationPresenter() {
        numberNotifyPresenter.getNumberNotify(PreferenceHelper.getString(User.UID), new NumberNotifyPresenter.NumberNotificationsView() {
            @Override
            public void onSucessGetNumber(String number) {
                civMyAcountNew.setVisibility(View.VISIBLE);
                tvNumberNotify.setVisibility(View.VISIBLE);
                tvNumberNotify.setText(number);
            }

            @Override
            public void onSucessNumberZero() {
                civMyAcountNew.setVisibility(View.GONE);
                tvNumberNotify.setVisibility(View.GONE);
            }

            @Override
            public void onError() {
                civMyAcountNew.setVisibility(View.GONE);
                tvNumberNotify.setVisibility(View.GONE);
            }
        });
    }

    private void bottomSheet() {
        BottomSheetBehavior bsb = BottomSheetBehavior.from(mBottomSheet);
        bsb.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int i) {

            }

            @Override
            public void onSlide(@NonNull View view, float v) {
                stateBottomSheet = v;
            }
        });
        mTvTitleBottomSheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (stateBottomSheet == 0.0) {
                    bsb.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    bsb.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
            }
        });

    }

    void setData() {
        mTvName.setText(PreferenceHelper.getString(User.NOMBRE));
    }


    private void initAdapter() {
        ordersAdapter = new OrdersAdapter(new ArrayList<>());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecentMovementsRecyclerView.setLayoutManager(linearLayoutManager);
        mRecentMovementsRecyclerView.setAdapter(ordersAdapter);
    }

    @Override
    protected BasePresenter[] getPresenters() {
        return new BasePresenter[]{
                historyOrdersPresenter = new HistoryPresenter(this, this),
                customerBalancePresenter = new BalancePresenter(this, this),
                numberNotifyPresenter = new NumberNotifyPresenter(this)
        };
    }


    @Override
    public void onSuccessHistory(ArrayList<Order> orders) {
        ordersAdapter.updateList(orders);
    }

    @Override
    public void onGetDataUser(String currentBalance) {
        BalanceAdapter adapter = new BalanceAdapter(getApplicationContext(), currentBalance);

        mViewPager.setAdapter(adapter);
        mViewPager.setPadding(130, 0, 130, 0);
    }

    @Override
    public void onErrorConnection() {
        Toast.makeText(this, R.string.error_connection, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onErrorBalance(int resMsg) {
        Toast.makeText(this, resMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onErrorHistory(int resMsg) {
        Toast.makeText(this, resMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLoadResponse() {

    }

    @OnClick({R.id.iv_profilee, R.id.iv_profilee_2, R.id.mImgBtnCollect, R.id.mLayoutPay, R.id.mImgBtnBell})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_profilee:
                showDialog();
                break;
            case R.id.mImgBtnCollect:
                startActivity(new Intent(new Intent(this, SplitPaymentQr.class)));
                break;
            case R.id.mLayoutPay:
                startActivity(new Intent(this, OrderResumeActivity.class));
                break;
            case R.id.mImgBtnBell:
                startActivity(new Intent(this, NotificationsActivity.class));
                break;
        }
    }

    private void showDialog() {
        CloseSessionDialog closeSessionDialog = CloseSessionDialog.newInstance();
        closeSessionDialog.show(this.getSupportFragmentManager(), "closeSesionDialog");
    }

    @Override
    public void onBackPressed() {
        showDialog();
//        super.onBackPressed();
    }
}
