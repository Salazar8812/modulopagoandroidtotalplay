package com.totalplay.modulepayandroid.modules.notifications.model;

import java.util.Date;

public class NotificationNetwork {

    private Date fecha;
    private String id;
    private String idPayee; // a quien le debo
    private float monto;
    private String nombrePayee; // a quien le debo
    private String orden;
    private String referencia;
    private String status;
    private boolean vista;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrden() {
        return orden;
    }

    public void setOrden(String orden) {
        this.orden = orden;
    }

    public NotificationNetwork() {
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getIdPayee() {
        return idPayee;
    }

    public void setIdPayee(String idPayee) {
        this.idPayee = idPayee;
    }

    public float getMonto() {
        return monto;
    }

    public void setMonto(float monto) {
        this.monto = monto;
    }

    public String getNombrePayee() {
        return nombrePayee;
    }

    public void setNombrePayee(String nombrePayee) {
        this.nombrePayee = nombrePayee;
    }


    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isVista() {
        return vista;
    }

    public void setVista(boolean vista) {
        this.vista = vista;
    }
}
