package com.totalplay.modulepayandroid.modules.notifications.model;

public class NotificationLocal {

    private String id;
    private String date;
    private String monto;
    private String payee; // a quien le debo
    private String idPayee; // a quien le debo
    private String idOrder;
    private String ref;
    private String status;
    private boolean see;

    public NotificationLocal() {
    }

    public NotificationLocal(String id, String date, String monto, String payee, String idPayee, String idOrder, String ref, String status, boolean see) {
        this.id = id;
        this.date = date;
        this.monto = monto;
        this.payee = payee;
        this.idPayee = idPayee;
        this.idOrder = idOrder;
        this.ref = ref;
        this.status = status;
        this.see = see;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }

    public String getPayee() {
        return payee;
    }

    public void setPayee(String payee) {
        this.payee = payee;
    }

    public String getIdPayee() {
        return idPayee;
    }

    public void setIdPayee(String idPayee) {
        this.idPayee = idPayee;
    }

    public String getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(String idOrder) {
        this.idOrder = idOrder;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isSee() {
        return see;
    }

    public void setSee(boolean see) {
        this.see = see;
    }
}
