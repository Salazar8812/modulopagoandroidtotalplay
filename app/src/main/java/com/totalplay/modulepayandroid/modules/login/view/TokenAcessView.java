package com.totalplay.modulepayandroid.modules.login.view;

public interface TokenAcessView {
    void onSuccess(String tk);
    void onError(int resMsg);
}
