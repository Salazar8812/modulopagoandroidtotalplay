package com.totalplay.modulepayandroid.modules.login.view;

public interface updateFSView {
    void onUpdateToken();
    void onErrorUpdateToken(int msg);
}
