package com.totalplay.modulepayandroid.modules.main.models;

public class Order {

    private String bussiness;
    private String pay;
    private String date;

    public Order() {
    }

    public Order(String bussiness, String pay, String date) {
        this.bussiness = bussiness;
        this.pay = pay;
        this.date = date;
    }

    public String getBussiness() {
        return bussiness;
    }

    public void setBussiness(String bussiness) {
        this.bussiness = bussiness;
    }

    public String getPay() {
        return pay;
    }

    public void setPay(String pay) {
        this.pay = pay;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
