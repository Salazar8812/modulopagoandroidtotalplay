package com.totalplay.modulepayandroid.modules.pay.presenters;

import android.content.Intent;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.resources.background.WSBaseResponseInterface;
import com.resources.background.WSCallback;
import com.resources.mvp.BasePresenter;
import com.resources.utils.MessageUtils;
import com.totalplay.modulepayandroid.R;
import com.totalplay.modulepayandroid.Utils.Constantes;
import com.totalplay.modulepayandroid.background.WSManagerNewEnvironment;
import com.totalplay.modulepayandroid.background.models.Request.Pay.PayRequest;
import com.totalplay.modulepayandroid.background.models.Response.DataQR.PayeeResponse;
import com.totalplay.modulepayandroid.background.models.Response.Pay.DataPayResponse;
import com.totalplay.modulepayandroid.background.webServices;
import com.totalplay.modulepayandroid.common.model.GetUserCallback;
import com.totalplay.modulepayandroid.common.model.pojo.User;
import com.totalplay.modulepayandroid.data.PreferenceHelper;
import com.totalplay.modulepayandroid.firebase.login.FireStore;
import com.totalplay.modulepayandroid.modules.pay.view.activities.PaySuccessActivity;

import java.util.Random;

import static com.totalplay.modulepayandroid.background.webServices.HTK;

public class PayPresenter extends BasePresenter implements WSCallback {

    AppCompatActivity mAppCompatActivity;
    String mMonto;
    String mPayee;
    String mOrder;
    String mReference;
    String mPropina = "0.00";
    Boolean mFacturaChecked;

    private String RESPONSE_SERVICE_FACTURA = "";
    private String RESPONSE_FIRESTORE_FACTURA = "";

    private static final String SEND_FIRESTORE = "send_firestore";
    private static final String ERROR_FIRESTORE = "request_firestore";
    private static final String SUCESS_FIRESTORE = "sucess_firestore";

    private static final String SEND_SERVICE = "send_service";
    private static final String ERROR_SERVICE = "request_service";
    private static final String SUCESS_SERVICE = "sucess_service";

    String mServiceStatus = "up";
    private static final String FLINK = "flink";
    private static final String IPTV = "iptv";
    private static final String USER = "users";

    private static final String AURELIO_ID = "8615000000218";
    private static final String IPTV_ID = "100002303";
    private static final String EDUARDO_ID = "8615000000053";
    private static final String FlINK_ID = "100002301";

    private static String TYPE_TRANSACTION = "";

    private FireStore mFireStore;
    private View view;

    public PayPresenter(AppCompatActivity appCompatActivity, View view) {
        super(appCompatActivity);
        this.mAppCompatActivity = appCompatActivity;
        mFireStore = new FireStore();
        this.view = view;
    }

    public void getDataPay(String idPay) {
        RESPONSE_SERVICE_FACTURA = SEND_SERVICE;
        WSManagerNewEnvironment.init().settings(mContext, this).requestWs(PayeeResponse.class,
                WSManagerNewEnvironment.WS.DATA_PAY,
                webServices.services().getDataPay(idPay));
    }


    public void userFactura() {
        RESPONSE_FIRESTORE_FACTURA = SEND_FIRESTORE;
        mFireStore.getUser(PreferenceHelper.getString(User.UID), new GetUserCallback() {
            @Override
            public void onSuccess(User userFB) {
                if (userFB.getFacturacion() != null) {
                    RESPONSE_FIRESTORE_FACTURA = SUCESS_FIRESTORE;
                    view.onSuccessUserFactura(true);
                    checkResponse();
                } else {
                    RESPONSE_FIRESTORE_FACTURA = ERROR_FIRESTORE;
                    view.onSuccessUserFactura(false);
                    checkResponse();
                }
            }

            @Override
            public void onError(String e) {
                RESPONSE_FIRESTORE_FACTURA = ERROR_FIRESTORE;
                view.onErrorFactura();
                checkResponse();
            }
        });

    }

    private void checkResponse() {
        if (!RESPONSE_SERVICE_FACTURA.isEmpty() && !RESPONSE_FIRESTORE_FACTURA.isEmpty()) {
            if (!RESPONSE_SERVICE_FACTURA.equals(SEND_SERVICE) && !RESPONSE_FIRESTORE_FACTURA.equals(SEND_FIRESTORE)) {
                view.goneProgress();
            }
        }
    }


    public void CreatePayOrdersP2P(String mAmount, String mIdPayee, String order) {
        mMonto = mAmount;
        mPayee = mIdPayee;
        mOrder = order;
        TYPE_TRANSACTION = USER;
        MessageUtils.progress(mAppCompatActivity, "Cargando");
        WSManagerNewEnvironment.init().settings(mContext, this).
                requestWs(
                        DataPayResponse.class,
                        WSManagerNewEnvironment.WS.CREATEPAYORDERP2P,
                        webServices.servicesAuth().createPay(getPayRequest(mIdPayee, false, "0", order, "pago usuario")));
    }

    public void CreatePayOrderC2B(String mAmount, String mIdPayee, String order, String propina, boolean facturachecked) {
        mMonto = mAmount;
        mPayee = mIdPayee;
        mOrder = order;
        mPropina = propina;
        mFacturaChecked = facturachecked;
        MessageUtils.progress(mAppCompatActivity, "Cargando");
        if (mIdPayee.equals("100002303")) {
            //iptv
            TYPE_TRANSACTION = IPTV;
            WSManagerNewEnvironment.init().settings(mContext, this).
                    requestWs(
                            DataPayResponse.class,
                            WSManagerNewEnvironment.WS.CREATEPAYORDERP2P,
                            webServices.servicesAuth().createPay(getPayRequest(mIdPayee, facturachecked, propina, order, "pago Hotel") ));
        } else {
            //flink
            TYPE_TRANSACTION = FLINK;
            WSManagerNewEnvironment.init().settings(mContext, this).
                    requestWs(
                            DataPayResponse.class,
                            WSManagerNewEnvironment.WS.CREATEPAYORDERP2P,
                            webServices.servicesAuth().createPay(getPayRequest(mIdPayee, facturachecked, propina, order, "pago chowa") ));
        }
    }

    private PayRequest getPayRequest(String mIdPayee, boolean facturachecked, String propina, String order, String ref) {
        PayRequest payRequest = new PayRequest();
        payRequest.setPayer(PreferenceHelper.getString(User.UID));
        payRequest.setPayee(mIdPayee);
        payRequest.setFactura(facturachecked);
        payRequest.setTip(Float.parseFloat(propina));
        payRequest.setReference(ref);
        mReference = ref;
        payRequest.setOrder(order);
        payRequest.setAmount(Float.parseFloat(mMonto));
        return payRequest;
    }

    @Override
    public void onRequestWS(String requestUrl) {

    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        switch (requestUrl) {
            case WSManagerNewEnvironment.WS.CREATEPAYORDERP2P:
                MessageUtils.stopProgress();
                servicesWork(baseResponse);
                break;
            case WSManagerNewEnvironment.WS.DATA_PAY:
                MessageUtils.stopProgress();
                serviceDataPay(baseResponse);
        }

    }

    private void serviceDataPay(WSBaseResponseInterface baseResponse) {
        try {
            PayeeResponse payeeResponse = (PayeeResponse) baseResponse;
            RESPONSE_SERVICE_FACTURA = SUCESS_SERVICE;
            view.onSuccessBusinesstData(payeeResponse.getNombre(), payeeResponse.isEmiteFactura());
            checkResponse();
        } catch (Exception e) {
            RESPONSE_SERVICE_FACTURA = ERROR_SERVICE;
            view.onErrorGetData(R.string.error_response_payee_response_demo);
            checkResponse();
        }

    }

    private void servicesWork(WSBaseResponseInterface baseResponse) {
        try {
            DataPayResponse payResponse = (DataPayResponse) baseResponse;
            if (payResponse != null) {
                selectOnSucess();
            } else {
                view.onErrorPay(R.string.error_pay);
            }
        } catch (Exception e) {
            view.onErrorPay(R.string.error_pay_catch);
            MessageUtils.toast(mAppCompatActivity, "demof");
        }
    }

    private void selectOnSucess() {
        switch (mPayee) {
            case AURELIO_ID:
            case EDUARDO_ID:
            case IPTV_ID:
                onSuccess();
                break;
            case FlINK_ID:
                onSuccessPropina();
                break;
            default:
                break;
        }

        RESPONSE_SERVICE_FACTURA = SUCESS_SERVICE;
    }

    @Override
    public void onErrorLoadResponse(String requestUrl, String messageError) {
        MessageUtils.stopProgress();
        if (requestUrl.equals(WSManagerNewEnvironment.WS.DATA_PAY)) {
            view.onErrorGetData(R.string.error_pay);
            RESPONSE_SERVICE_FACTURA = ERROR_SERVICE;
            checkResponse();
        } else {
            MessageUtils.toast(mAppCompatActivity, "Error en el servicio PAY");
        }
    }

    @Override
    public void onErrorConnection() {
        MessageUtils.stopProgress();
    }

    private void onSuccessPropina() {
        view.onSuccessPayTip(mMonto, mPayee, mReference, mPropina, mOrder, TYPE_TRANSACTION);
    }

    private void onSuccess() {
        view.onSucessPay(mMonto, mPayee, mReference, TYPE_TRANSACTION);
    }

    public interface View {
        //response firebase
        void onSuccessUserFactura(boolean b);

        void onErrorFactura();

        //response service getData
        void onSuccessBusinesstData(String user, boolean isFatura);

        void onErrorGetData(int resMsg);

        //response service pay
        void onSucessPay(String monto, String mMonto, String mPayee, String mReference);

        void onSuccessPayTip(String mMonto, String mPayee, String mReference, String mPropina, String mOrder, String typeTransaction);

        void onErrorPay(int resMsg);

        //error generico
        void onErrorConnection();

        void goneProgress();
    }

}
