package com.totalplay.modulepayandroid.modules.divideExpense.models.request;

import com.google.gson.annotations.SerializedName;

public class PayersItem{

	@SerializedName("amount")
	private double amount;

	@SerializedName("payer")
	private String payer;

	public void setAmount(double amount){
		this.amount = amount;
	}

	public double getAmount(){
		return amount;
	}

	public void setPayer(String payer){
		this.payer = payer;
	}

	public String getPayer(){
		return payer;
	}
}