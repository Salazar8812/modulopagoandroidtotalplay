package com.totalplay.modulepayandroid.modules.divideExpense.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.totalplay.modulepayandroid.Adapters.DivideExpenseAdapter;
import com.totalplay.modulepayandroid.FragmentsDivideExpense.FragmentFrequent;
import com.totalplay.modulepayandroid.MainAppCompatActivity;
import com.totalplay.modulepayandroid.R;
import com.totalplay.modulepayandroid.common.model.pojo.User;
import com.totalplay.modulepayandroid.data.PreferenceHelper;
import com.totalplay.modulepayandroid.modules.divideExpense.models.Contact;
import com.totalplay.modulepayandroid.modules.divideExpense.view.DataUsers;
import com.totalplay.modulepayandroid.modules.divideExpense.view.fragments.FragmentAll;

import java.text.DecimalFormat;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DivideExpenseActivity extends MainAppCompatActivity implements DataUsers {
    public static final String MONTO = "monto";
    public static final String REFERENCIA = "reference";
    public static final String ORDER = "order";

    private static final String FRAGMENT_FAVORITES = "favorites";
    private static final String FRAGMENT_ALL = "all";

    @BindView(R.id.mTabLayout)
    TabLayout mTabLayout;
    @BindView(R.id.mViewPager)
    ViewPager mViewPager;
    @BindView(R.id.tv_monto_divide_expense)
    TextView tvMontoDivideExpense;
    @BindView(R.id.tv_cb_divide_expense)
    TextView tvCbDivideExpense;

    private DivideExpenseAdapter mDivideExpenseAdapter;
    private ArrayList<Contact> contacts;
    private ArrayList<Contact> contactsFinalAll;
    private ArrayList<Contact> contactsFinal;
    private ArrayList<Contact> contactsFinalFavorites;

    private float monto = 400f;
    private String mReferencia = "reference";
    private String mOrder = "order";
    private boolean cbYo = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.activity_divide_expense);
        ButterKnife.bind(this);
        getIntents();
        initContacts();
        setData();
        mDivideExpenseAdapter = new DivideExpenseAdapter(getSupportFragmentManager());
        mDivideExpenseAdapter.addFragment(FragmentAll.newInstance(), "Todos");
        mDivideExpenseAdapter.addFragment(FragmentFrequent.newInstance(), "Frecuentes");
        mViewPager.setAdapter(mDivideExpenseAdapter);
        mTabLayout.setupWithViewPager(mViewPager);

    }

    private void initContacts() {
        this.contacts = new ArrayList<>();
        this.contactsFinalAll = new ArrayList<>();
        this.contactsFinal = new ArrayList<>();
        this.contactsFinalFavorites = new ArrayList<>();
    }

    void setData() {
        tvCbDivideExpense.setText(PreferenceHelper.getString(User.NOMBRE));
    }


    private void getIntents() {
        monto = getIntent().getExtras().getFloat(MONTO);
        mReferencia = getIntent().getStringExtra(REFERENCIA);
        mOrder = getIntent().getStringExtra(ORDER);
        DecimalFormat df = new DecimalFormat("#.00");
        tvMontoDivideExpense.setText("$" + df.format(monto));
    }

    @Override
    protected void onResume() {
        super.onResume();
        onSetActivity(this);
        onSetTitle("DIVIDIR CUENTA");
    }

    @OnClick(R.id.mDividirButton)
    void OnClick(View v) {
        switch (v.getId()) {
            case R.id.mDividirButton:
                if (contactsFinal != null) {
                    if (!contactsFinal.isEmpty()) {
                        Intent i = new Intent(this, PersonDivideActivity.class);
                        i.putParcelableArrayListExtra(PersonDivideActivity.USERS, contactsFinal);
                        i.putExtra(PersonDivideActivity.MONTO, monto);
                        i.putExtra(PersonDivideActivity.REFERENCE, mReferencia);
                        i.putExtra(PersonDivideActivity.ORDER, mOrder);
                        i.putExtra(PersonDivideActivity.CB_YO, cbYo);
                        startActivity(i);
                    } else {
                        Toast.makeText(this, "No ha seleccionado integrantes", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, "No ha seleccionado integrantes", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }
    }


    @OnClick(R.id.cb_yo_divide_expense)
    public void onClick() {
        cbYo = !cbYo;
    }


    @Override
    public void onDataUsers(ArrayList<Contact> contacts, String fragment) {
        managerContactsRepeat(contacts, fragment);
    }

    private void managerContactsRepeat(ArrayList<Contact> contacts, String fragmentProvider) {
        if (fragmentProvider.equals(FRAGMENT_ALL)) {
            contactsFinalAll.clear();
            contactsFinalAll.addAll(getContactsIspay(contacts));
        }
        if (fragmentProvider.equals(FRAGMENT_FAVORITES)) {
            contactsFinalFavorites.clear();
            contactsFinalFavorites.addAll(getContactsIspay(contacts));
        }
        comparaListas(contactsFinalAll, contactsFinalFavorites);
    }

    private void comparaListas(ArrayList<Contact> contactsFinalAll, ArrayList<Contact> contactsFinalFavorites) {
        ArrayList<Contact> contactsNew = new ArrayList<>();
        for (Contact contact : contactsFinalFavorites) {
            if (!estaEnLaLista(contactsFinalAll, contact)) {
                contactsNew.add(contact);
            }
        }
        contactsNew.addAll(contactsFinalAll);

        this.contactsFinal.clear();
        this.contactsFinal.addAll(contactsNew);
    }

    private boolean estaEnLaLista(ArrayList<Contact> contactsFinalAll, Contact contact) {
        for (Contact contact1 : contactsFinalAll) {
            if (contact1.getId().equals(contact.getId())) {
                return true;
            }
        }
        return false;
    }

    private ArrayList<Contact> getContactsIspay(ArrayList<Contact> contacts) {
        ArrayList<Contact> contactsNew = new ArrayList<>();
        for (Contact contact : contacts) {
            if (contact.isSelectPay()) {
                contactsNew.add(contact);
            }
        }
        return contactsNew;
    }
}
