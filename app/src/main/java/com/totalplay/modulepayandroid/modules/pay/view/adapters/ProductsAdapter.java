package com.totalplay.modulepayandroid.modules.pay.view.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.totalplay.modulepayandroid.R;
import com.totalplay.modulepayandroid.modules.pay.models.ProductsOrder;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ItemViewHolder> {


    private ArrayList<ProductsOrder> list;

    public ProductsAdapter(ArrayList<ProductsOrder> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_person_order, parent, false);
        return new ItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        final ProductsOrder productsOrderItem = list.get(position);
        holder.tvNumberProductItemPersonOrder.setText(productsOrderItem.getCuantity());
        holder.tvDescriptionProductItemPersonOrder.setText(productsOrderItem.getDescription());
        holder.tvPrriceProductItemPersonOrder.setText(productsOrderItem.getPrice());
    }

    public void updateList(ArrayList<ProductsOrder> list){
        this.list.clear();
        this.list.addAll(list);
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_number_product_item_person_order)
        TextView tvNumberProductItemPersonOrder;
        @BindView(R.id.tv_description_product_item_person_order)
        TextView tvDescriptionProductItemPersonOrder;
        @BindView(R.id.tv_prrice_product_item_person_order)
        TextView tvPrriceProductItemPersonOrder;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
