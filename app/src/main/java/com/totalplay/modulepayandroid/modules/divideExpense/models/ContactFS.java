package com.totalplay.modulepayandroid.modules.divideExpense.models;

public class ContactFS {

    String nombre;
    String id;
    boolean favorito;

    public ContactFS() {
    }

    public ContactFS(String nombre, String id, boolean favorito) {
        this.nombre = nombre;
        this.id = id;
        this.favorito = favorito;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isFavorito() {
        return favorito;
    }

    public void setFavorito(boolean favorito) {
        this.favorito = favorito;
    }
}
