package com.totalplay.modulepayandroid.modules.pay.models;

public class ProductsOrder {

    private String cuantity;
    private String description;
    private String price;

    public ProductsOrder() {
    }

    public ProductsOrder(String cuantity, String description, String price) {
        this.cuantity = cuantity;
        this.description = description;
        this.price = price;
    }

    public String getCuantity() {
        return cuantity;
    }

    public void setCuantity(String cuantity) {
        this.cuantity = cuantity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
