package com.totalplay.modulepayandroid.modules.divideExpense.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Contact implements Parcelable {

    String name;
    String id;
    boolean selectPay;
    boolean favorite;
    double amount;

    public Contact() {
    }

    public Contact(String name, String id, boolean selectPay, boolean favorite) {
        this.name = name;
        this.id = id;
        this.selectPay = selectPay;
        this.favorite = favorite;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    protected Contact(Parcel in) {
        name = in.readString();
        id = in.readString();
        selectPay = in.readByte() != 0;
        favorite = in.readByte() != 0;
    }

    public static final Creator<Contact> CREATOR = new Creator<Contact>() {
        @Override
        public Contact createFromParcel(Parcel in) {
            return new Contact(in);
        }

        @Override
        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelectPay() {
        return selectPay;
    }

    public void setSelectPay(boolean selectPay) {
        this.selectPay = selectPay;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(id);
        parcel.writeByte((byte) (selectPay ? 1 : 0));
        parcel.writeByte((byte) (favorite ? 1 : 0));
    }
}
