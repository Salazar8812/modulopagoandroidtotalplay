package com.totalplay.modulepayandroid.modules.pay.view.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.totalplay.modulepayandroid.MainAppCompatActivity;
import com.totalplay.modulepayandroid.R;
import com.totalplay.modulepayandroid.Utils.Constantes;
import com.totalplay.modulepayandroid.common.model.pojo.User;
import com.totalplay.modulepayandroid.data.PreferenceHelper;
import com.totalplay.modulepayandroid.modules.main.view.activities.MyAccountNewActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SplitPaymentQr extends MainAppCompatActivity {
    @BindView(R.id.mQrImageView)
    ImageView mQrImageView;

    @BindView(R.id.mTitleCommerceTextView)
    TextView mTitleCommerceTextView;

    /*var para cobro generico*/
    private String varAmount = "100";
    private String varOrderId = "123456";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.activity_split_payment_qr);
        ButterKnife.bind(this);
        generateQR();
        setData();
    }

    /*
     *           "https://totalpay.page.link/?link=http://www.totalpay.com/?idPayee%3D"+
     *           Constantes.mIDPayer + " %26method%3Dpay%26amount%3D" + 100 +
     *           "%26fixedAmount%3D" + true + "%26order%3D" + 3879487329874 +
     *           "%26apn%3Dcom.totalplay.modulepayandroid%26ibi%3Dcom.TotalPlay.ModuloPagoIOSTP&apn=com.totalplay.modulepayandroid&efr=1"
     *
     *
     * */

    private String getStrQR(Boolean fixedAmount) {
        String base = "https://totalpay.page.link/?link=http://www.totalpay.com/?idPayee%3D";
        String strMehtodPayAMount = "%26method%3Dpay%26amount%3D";
        String amount = varAmount;
        String strFixedAmount = "%26fixedAmount%3D";
        String strOrder = "%26order%3D";
        String order = varOrderId;
        String end = "%26apn%3Dcom.totalplay.modulepayandroid%26ibi%3Dcom.TotalPlay.ModuloPagoIOSTP&apn=com.totalplay.modulepayandroid&efr=1";

        return base + PreferenceHelper.getString(User.UID)
                + strMehtodPayAMount
                + amount
                + strFixedAmount
                + fixedAmount
                + strOrder
                + order
                + end;
    }

    private String getStrQR2(Boolean fixedAmount) {
        String base = "https://totalgo.totalplay.com.mx/section/pago?";
        String strIdPayee = "idPayee=";
        String strAmount = "amount=";
        String strFixedAmount = "fixedAmount=";
        String strOrder = "order=";
        String amp = "&";
        String amount = varAmount;
        String order = varOrderId;

        return base
                + strIdPayee + PreferenceHelper.getString(User.UID) + amp
                + strAmount + amount + amp
                + strFixedAmount + fixedAmount + amp
                + strOrder + order;
    }



    void generateQR() {
        try {
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = null;
            if (PreferenceHelper.getString(User.UID).substring(0, 2).equals("86")) {
                /*es persona es*/
                bitmap = barcodeEncoder.encodeBitmap(getStrQR2(false),
                        BarcodeFormat.QR_CODE, 500, 500);
            } else {
                /*es empresa es */
                bitmap = barcodeEncoder.encodeBitmap(getStrQR2(true),
                        BarcodeFormat.QR_CODE, 500, 500);

            }
            //Bitmap bitmap = barcodeEncoder.encodeBitmap(Constantes    .mIdPayer, BarcodeFormat.QR_CODE, 500, 500);
            mQrImageView.setImageBitmap(bitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    void setData() {
        mTitleCommerceTextView.setText(PreferenceHelper.getString(User.NOMBRE));
    }

    @Override
    protected void onResume() {
        super.onResume();
        onSetActivity(this);
        onSetTitle("COBRAR CON QR");
    }

    @OnClick({R.id.mSalirButton, R.id.mShareButton})
    void OnClick(View v) {

        switch (v.getId()) {
            case R.id.mSalirButton:
                startActivity(new Intent(this, MyAccountNewActivity.class));
                break;

            case R.id.mShareButton:
                shareQR();
                break;
            default:
                break;
        }

    }

    void shareQR() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, "TotalPay");
        intent.putExtra(Intent.EXTRA_TEXT, "Codigo QR");
        startActivity(Intent.createChooser(intent, "Selecciona:"));
    }

}