package com.totalplay.modulepayandroid.modules.notifications.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.totalplay.modulepayandroid.MainAppCompatActivity;
import com.totalplay.modulepayandroid.R;
import com.totalplay.modulepayandroid.common.model.pojo.User;
import com.totalplay.modulepayandroid.data.PreferenceHelper;
import com.totalplay.modulepayandroid.modules.notifications.model.NotificationLocal;
import com.totalplay.modulepayandroid.modules.notifications.presenters.NotificationsPresenter;
import com.totalplay.modulepayandroid.modules.notifications.view.adapters.NotificationsNoVistasAdapter;
import com.totalplay.modulepayandroid.modules.notifications.view.adapters.NotificationsVistasAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationsActivity extends MainAppCompatActivity implements NotificationsVistasAdapter.onItemClickListener, NotificationsPresenter.View, NotificationsNoVistasAdapter.onItemClickListener {

    @BindView(R.id.rv_notifications_view)
    RecyclerView rvNotificationsView;
    @BindView(R.id.rv_notifications_no_view)
    RecyclerView rvNotificationsNoView;
    @BindView(R.id.tv_vistas_notifications)
    TextView tvVistasNotifications;
    @BindView(R.id.tv_sin_notificationes_vistas)
    TextView tvSinNotificationesVistas;
    @BindView(R.id.sv_notificaciones)
    ScrollView svNotificaciones;
    @BindView(R.id.tv_sin_notificaciones_general)
    TextView tvSinNotificacionesGeneral;

    @BindView(R.id.pb_notifications)
    ProgressBar pbNotifications;
    @BindView(R.id.shimeer_progress_notifications)
    ShimmerFrameLayout shimeerProgressNotifications;

    private NotificationsVistasAdapter notificationsVistasAdapter;
    private NotificationsNoVistasAdapter notificationsNoVistasAdapter;

    private NotificationsPresenter notificationsPresenter;

    ArrayList<NotificationLocal> notificationVistas = new ArrayList<>();
    ArrayList<NotificationLocal> notificationNovistas = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.activity_notifications);
        ButterKnife.bind(this);
        progressvisible(true);
        initiPresenter();
        initAdapters();
    }

    private void progressvisible(boolean visible) {
        pbNotifications.setVisibility(visible ? View.VISIBLE : View.GONE);
        shimeerProgressNotifications.setVisibility(visible ? View.VISIBLE : View.GONE);
        svNotificaciones.setVisibility(visible ? View.GONE : View.VISIBLE);

        if (visible){
            shimeerProgressNotifications.startShimmerAnimation();
        }
        else
            shimeerProgressNotifications.stopShimmerAnimation();

    }

    private void hayNotificationes(boolean areNotify) {
        tvSinNotificacionesGeneral.setVisibility(areNotify ? View.GONE : View.VISIBLE);
        svNotificaciones.setVisibility(areNotify ? View.VISIBLE : View.GONE);
    }


    @Override
    protected void onResume() {
        super.onResume();
        onSetActivity(this);
        notificationsPresenter.getRequests(PreferenceHelper.getString(User.UID));
        onSetTitle("NOTIFICACIONES");
    }

    private void initiPresenter() {
        notificationsPresenter = new NotificationsPresenter(this, this);
    }


    private void initAdapters() {
        rvNotificationsView.setLayoutManager(new LinearLayoutManager(this));
        notificationsVistasAdapter = new NotificationsVistasAdapter(notificationVistas, this);
        rvNotificationsView.setAdapter(notificationsVistasAdapter);

        rvNotificationsNoView.setLayoutManager(new LinearLayoutManager(this));
        notificationsNoVistasAdapter = new NotificationsNoVistasAdapter(notificationNovistas, this);
        rvNotificationsNoView.setAdapter(notificationsNoVistasAdapter);
    }

    @Override
    public void onItemClickVistas(View v) {
        int item = (int) v.getTag();
        NotificationLocal notificationLocal = notificationVistas.get(item);
        startNewActivity(notificationLocal);
    }

    @Override
    public void onItemClick(View v) {
        int item = (int) v.getTag();
        NotificationLocal notificationLocal = notificationNovistas.get(item);
        startNewActivity(notificationLocal);
    }

    private void startNewActivity(NotificationLocal notificationLocal) {
        Intent intent = new Intent(this, PaymentRequestActivity.class);
        intent.putExtra(PaymentRequestActivity.FRIEND, notificationLocal.getPayee());
        intent.putExtra(PaymentRequestActivity.DATE, notificationLocal.getDate());

        intent.putExtra(PaymentRequestActivity.QUANTITY, notificationLocal.getMonto());
        intent.putExtra(PaymentRequestActivity.REFENRENCE, notificationLocal.getRef());
        intent.putExtra(PaymentRequestActivity.ORDER, notificationLocal.getIdOrder());
        intent.putExtra(PaymentRequestActivity.ID_PAYEER, PreferenceHelper.getString(User.UID));
        intent.putExtra(PaymentRequestActivity.ID_PAYEE, notificationLocal.getIdPayee());
        intent.putExtra(PaymentRequestActivity.ID_NOTIFY, notificationLocal.getId());
        intent.putExtra(PaymentRequestActivity.SEE, notificationLocal.isSee());
        startActivity(intent);
    }

    @Override
    public void onGetNotificationsLocal(ArrayList<NotificationLocal> nVistas, ArrayList<NotificationLocal> nNoVistas) {

        progressvisible(false);
        hayNotificationes(true);

        tvVistasNotifications.setVisibility(nVistas.isEmpty() ? View.VISIBLE : View.GONE);
        tvSinNotificationesVistas.setVisibility(nNoVistas.isEmpty() ? View.VISIBLE : View.GONE);

        notificationsVistasAdapter.update(nVistas);
        notificationsNoVistasAdapter.update(nNoVistas);
    }

    @Override
    public void onError(String msg) {
        progressvisible(false);
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onNotificationsEmpty() {
        progressvisible(false);
        hayNotificationes(false);
    }
}