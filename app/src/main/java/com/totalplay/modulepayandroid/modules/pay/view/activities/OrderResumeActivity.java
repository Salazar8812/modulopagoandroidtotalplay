package com.totalplay.modulepayandroid.modules.pay.view.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.resources.utils.MessageUtils;
import com.totalplay.modulepayandroid.data.PreferenceHelper;
import com.totalplay.modulepayandroid.modules.pay.view.adapters.ProductsAdapter;
import com.totalplay.modulepayandroid.MainAppCompatActivity;
import com.totalplay.modulepayandroid.R;
import com.totalplay.modulepayandroid.Utils.Constantes;
import com.totalplay.modulepayandroid.common.Constants;
import com.totalplay.modulepayandroid.common.model.GetUserCallback;
import com.totalplay.modulepayandroid.common.model.pojo.User;
import com.totalplay.modulepayandroid.modules.pay.models.ProductsOrder;
import com.totalplay.modulepayandroid.modules.pay.models.QRparams;
import com.totalplay.modulepayandroid.firebase.login.FireStore;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OrderResumeActivity extends MainAppCompatActivity {

    @BindView(R.id.tv_number_table_order_resume_value)
    TextView tvNumberTableOrderResumeValue;
    @BindView(R.id.tv_number_person_order_resume_value)
    TextView tvNumberPersonOrderResumeValue;
    @BindView(R.id.tv_money_total_order_resume_value)
    TextView tvMoneyTotalOrderResumeValue;
    @BindView(R.id.tv_money_total_total_order_resume_value)
    TextView tvMoneyTotalTotalOrderResumeValue;
    @BindView(R.id.rv_order_resume)
    RecyclerView rvOrderResume;

    @BindView(R.id.pb_order_resume)
    ProgressBar pbOrderResume;
    @BindView(R.id.tv_loading_order_resume)
    TextView tvLoadingOrderResume;
    @BindView(R.id.sv_order_resume)
    ScrollView svOrderResume;

    private QRparams qRparams;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.activity_order_resume);
        ButterKnife.bind(this);
        initAdapter();
        visivilityElements(true);
        validateCalling();
    }

    private void visivilityElements(Boolean b) {
        if (b) {
            pbOrderResume.setVisibility(View.VISIBLE);
            tvLoadingOrderResume.setVisibility(View.VISIBLE);
            svOrderResume.setVisibility(View.GONE);
        } else {
            pbOrderResume.setVisibility(View.GONE);
            tvLoadingOrderResume.setVisibility(View.GONE);
            svOrderResume.setVisibility(View.VISIBLE);

        }
    }

    void validateCalling() {
        if (getIntent().getStringExtra("dynamicLink") != null) {
            //viene desde la camara
            qRparams = getQRparams(getIntent());
        } else {
            //viene del activity
            callQR();
        }
    }

    void callQR() {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        integrator.setPrompt("Escanear código QR");
        integrator.setCameraId(0);  // Use a specific camera of the device
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(true);
        integrator.initiateScan();
    }


    private String getTypeUser(String idUser) {
        if (idUser.substring(0, 2).equals("10")) {
            if (idUser.substring(idUser.length() - 2).equals("03")) {
                return Constants.IPTV;
            } else {
                return Constants.FLINK;
            }
        }
        return Constants.USERS;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                this.onBackPressed();
            } else {
                getStringContent2(result.getContents());
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void checkUser(QRparams qRparams) {
        startnewActivity(qRparams);
    }


    private void getStringContent(String mCad) {
        Log.d("TAGGGG", "uri:" + Uri.parse(mCad));
        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(Uri.parse(mCad))
                .addOnSuccessListener(this, pendingDynamicLinkData -> {
                    if (pendingDynamicLinkData != null) {
                        qRparams = getQRparams(pendingDynamicLinkData.getLink());
                        setData(qRparams);
                    } else {
                        MessageUtils.toast(getApplicationContext(), "QR no válido. DinamicLink");
                        finish();
                    }
                })
                .addOnFailureListener(this, e ->
                        Log.w("Failure", "getDynamicLink:onFailure", e));
    }

    private void getStringContent2(String mCad) {
        if (mCad != null) {
            qRparams = getQRparams(Uri.parse(mCad));
            setData(qRparams);
        } else {
            MessageUtils.toast(getApplicationContext(), "QR no válido. DinamicLink");
            finish();
        }
    }


    private void setData(QRparams qRparams) {
        tvMoneyTotalTotalOrderResumeValue.setText("$"+qRparams.getMonto());
    }

    private QRparams getQRparams(Uri deepLink) {
        QRparams qRparams = new QRparams();
        try{
            if (deepLink != null) {
                qRparams.setIdUser(deepLink.getQueryParameter("idPayee"));
                qRparams.setMonto(deepLink.getQueryParameter("amount"));
                qRparams.setFixedAmount(deepLink.getQueryParameter("fixedAmount"));
                qRparams.setnOrder(deepLink.getQueryParameter("order"));
                qRparams.setTypeUser(getTypeUser(qRparams.getIdUser()));
                checkUser(qRparams);
                return qRparams;
            } else {
                MessageUtils.toast(OrderResumeActivity.this, "Ocurrio un error");
            }
        }catch (Exception e){
            Toast.makeText(this, "QR no válido", Toast.LENGTH_SHORT).show();
        }
        return qRparams;
    }

    private void startnewActivity(QRparams qRparams) {
        Intent intent = new Intent(this, PayActivity.class);
        intent.putExtra(PayActivity.ID_USER, qRparams.getIdUser());
        intent.putExtra(PayActivity.MONTO, qRparams.getMonto());
        intent.putExtra(PayActivity.FIXED_AMOUNT, qRparams.getFixedAmount());
        intent.putExtra(PayActivity.ORDER, qRparams.getnOrder());
        intent.putExtra(PayActivity.TYPE_USER, qRparams.getTypeUser());
        startActivity(intent);
    }

    private QRparams getQRparams(Intent intent) {
        String[] mParametersQR = intent.getStringArrayExtra("ParameterQR");
        QRparams qRparams = new QRparams();
        if (mParametersQR != null) {
            qRparams.setIdUser(mParametersQR[0]);
            qRparams.setMonto(mParametersQR[2]);
            qRparams.setFixedAmount(mParametersQR[3]);
            qRparams.setnOrder(mParametersQR[4]);
            qRparams.setTypeUser(getTypeUser(qRparams.getIdUser()));
            checkUser(qRparams);
        } else {
            MessageUtils.toast(OrderResumeActivity.this, "Ocurrio un error");
        }
        return qRparams;
    }


    private void initAdapter() {
        ArrayList<ProductsOrder> listProducts = new ArrayList<>();
        ProductsOrder productsOrder1;
        ProductsOrder productsOrder2;
        ProductsOrder productsOrder3;

        productsOrder1 = new ProductsOrder("2", "Hamburguesa", "$135.00");
        productsOrder2 = new ProductsOrder("1", "Cerveza xx", "$35.00");
        productsOrder3 = new ProductsOrder("3", "Tacos dorados", "$130.00");

        listProducts.add(productsOrder1);
        listProducts.add(productsOrder2);
        listProducts.add(productsOrder3);

        ProductsAdapter productsAdapter = new ProductsAdapter(listProducts);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvOrderResume.setLayoutManager(linearLayoutManager);
        rvOrderResume.setAdapter(productsAdapter);

    }

    @OnClick(R.id.btn_next_order_resume)
    public void onClick() {
        startnewActivity(qRparams);
    }
}