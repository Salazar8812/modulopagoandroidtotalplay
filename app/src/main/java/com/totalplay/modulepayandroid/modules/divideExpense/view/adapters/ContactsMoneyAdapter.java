package com.totalplay.modulepayandroid.modules.divideExpense.view.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.totalplay.modulepayandroid.R;
import com.totalplay.modulepayandroid.modules.divideExpense.models.Contact;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ContactsMoneyAdapter extends RecyclerView.Adapter<ContactsMoneyAdapter.ViewHolder> {
    private ArrayList<Contact> mContacts;

    public ContactsMoneyAdapter(ArrayList<Contact> mContacts) {
        this.mContacts = mContacts;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_person_pay_divide, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Contact contact = mContacts.get(position);
        holder.tvNamePersonPayDivide.setText(contact.getName());
        holder.tvAmountPersonPayDivide.setText("$" + contact.getAmount());
        holder.itemView.setTag(position);
    }

    @Override
    public int getItemCount() {
        return mContacts.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_item_person_pay_divide)
        CircleImageView ivItemPersonPayDivide;
        @BindView(R.id.tv_name_person_pay_divide)
        TextView tvNamePersonPayDivide;
        @BindView(R.id.iv_row_item_pay_divide)
        ImageView ivRowItemPayDivide;
        @BindView(R.id.tv_amount_person_pay_divide)
        TextView tvAmountPersonPayDivide;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
