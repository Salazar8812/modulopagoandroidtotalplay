package com.totalplay.modulepayandroid.modules.notifications.presenters;

import androidx.appcompat.app.AppCompatActivity;

import com.resources.background.WSBaseResponseInterface;
import com.resources.background.WSCallback;
import com.resources.mvp.BasePresenter;
import com.resources.utils.MessageUtils;
import com.totalplay.modulepayandroid.background.WSManagerNewEnvironment;
import com.totalplay.modulepayandroid.background.models.Request.Pay.PayRequest;
import com.totalplay.modulepayandroid.background.models.Response.Pay.DataPayResponse;
import com.totalplay.modulepayandroid.background.webServices;
import com.totalplay.modulepayandroid.common.model.pojo.User;
import com.totalplay.modulepayandroid.data.PreferenceHelper;

public class PayPayeePresenter extends BasePresenter implements WSCallback {

    private String mMonto;
    private String mPayee;
    private String mOrder;
    private String mReference;

    private PayPayeePresenter.View view;

    public PayPayeePresenter(AppCompatActivity appCompatActivity, View view ) {
        super(appCompatActivity);
        this.view = view;
    }

    public void CreatePayOrdersP2P(String mAmount, String mIdPayee, String order, String ref) {
        mMonto = mAmount;
        mPayee = mIdPayee;
        mOrder = order;
        mReference = ref;
        WSManagerNewEnvironment.init().settings(mContext, this).
                requestWs(
                        DataPayResponse.class,
                        WSManagerNewEnvironment.WS.CREATEPAYORDERP2P,
                        webServices.servicesAuth().createPay(getPayRequest( mAmount , mIdPayee,  order , ref , false, "0")));
    }

    private PayRequest getPayRequest(String mAmount, String mIdPayee, String order, String ref, boolean factura, String tip) {
        PayRequest payRequest = new PayRequest();
        payRequest.setPayer(PreferenceHelper.getString(User.UID));
        payRequest.setPayee(mIdPayee);
        payRequest.setFactura(factura);
        payRequest.setTip(Float.parseFloat(tip));
        payRequest.setReference(ref);
        payRequest.setOrder(order);
        payRequest.setAmount(Float.parseFloat(mAmount));
        return payRequest;
    }


    @Override
    public void onRequestWS(String requestUrl) {

    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        try {
            DataPayResponse payResponse = (DataPayResponse) baseResponse;
            if (payResponse != null) {
                onSuccess();
            } else {
                view.onErrorPay("Error en el servicio pago");
            }
        } catch (Exception e) {
            view.onErrorPay("Error en el servicio pago catch");
            MessageUtils.toast(mAppCompatActivity, "demof");
        }

    }

    private void onSuccess() {
        view.onSucessPay(mMonto, mPayee, mReference, mOrder);
    }

    @Override
    public void onErrorLoadResponse(String requestUrl, String messageError) {
        view.onErrorPay(messageError);
    }

    @Override
    public void onErrorConnection() {

    }

    public interface View{
        void onSucessPay(String monto,  String mPayee, String mReference, String mOrder);
        void onErrorPay(String msg);
    }
}
