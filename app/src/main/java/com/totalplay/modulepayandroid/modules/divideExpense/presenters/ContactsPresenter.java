package com.totalplay.modulepayandroid.modules.divideExpense.presenters;

import androidx.fragment.app.FragmentActivity;

import com.totalplay.modulepayandroid.modules.divideExpense.models.Contact;
import com.totalplay.modulepayandroid.modules.divideExpense.models.ContactFS;
import com.totalplay.modulepayandroid.modules.divideExpense.models.contactsFSCallback;
import com.totalplay.modulepayandroid.firebase.login.FireStore;

import java.util.ArrayList;

public class ContactsPresenter {

    private FragmentActivity mAppcompactActivity;
    private ContactsPresenter.View view;
    private FireStore mFireStore;

    public ContactsPresenter(FragmentActivity mAppcompactActivity, View view) {
        this.mAppcompactActivity = mAppcompactActivity;
        this.view = view;
        mFireStore = new FireStore();
    }

    public void getContacts(String uid){
        mFireStore.getContactos(uid, new contactsFSCallback(){
            @Override
            public void onSuccessgetContacts(ArrayList<ContactFS> listcontactFS) {
                ArrayList<Contact> contacts = new ArrayList<>();
                for(ContactFS contactFS : listcontactFS){
                    Contact contact = new Contact();
                    contact.setName(contactFS.getNombre());
                    contact.setFavorite(contactFS.isFavorito());
                    contact.setId(contactFS.getId());
                    contact.setSelectPay(false);
                    contacts.add(contact);
                }
                view.onGetContacts(contacts);
            }

            @Override
            public void onError() {
                view.onError("fallo al consultar FS");
            }
        });
    }

    public void getContactsFav(String uid){
        mFireStore.getContactos(uid, new contactsFSCallback(){
            @Override
            public void onSuccessgetContacts(ArrayList<ContactFS> listcontactFS) {
                ArrayList<Contact> contacts = new ArrayList<>();
                for(ContactFS contactFS : listcontactFS){
                    Contact contact = new Contact();
                    contact.setName(contactFS.getNombre());
                    contact.setFavorite(contactFS.isFavorito());
                    contact.setId(contactFS.getId());
                    contact.setSelectPay(false);
                    if(contactFS.isFavorito()){
                        contacts.add(contact);
                    }
                }
                view.onGetContacts(contacts);
            }

            @Override
            public void onError() {
                view.onError("fallo al consultar FS");
            }
        });
    }

    public interface View{
        void onGetContacts(ArrayList<Contact> contacts);
        void onError(String msg);
    }
}
