package com.totalplay.modulepayandroid.modules.notifications.presenters;

import androidx.appcompat.app.AppCompatActivity;

import com.totalplay.modulepayandroid.modules.notifications.model.NotificationLocal;
import com.totalplay.modulepayandroid.modules.notifications.model.NotificationNetwork;
import com.totalplay.modulepayandroid.modules.notifications.model.RequestFSCallback;
import com.totalplay.modulepayandroid.firebase.login.FireStore;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class NotificationsPresenter {
    private static final String VISTAS = "vistas";
    private static final String NO_VISTAS = "no_vistas";
    private AppCompatActivity appCompatActivity;
    private NotificationsPresenter.View view;
    private FireStore mFireStore;

    public NotificationsPresenter(AppCompatActivity appCompatActivity, View view) {
        this.appCompatActivity = appCompatActivity;
        this.view = view;
        mFireStore = new FireStore();
    }

    public void getRequests(String uid) {
        mFireStore.getRequestsReceived(uid, new RequestFSCallback() {
            @Override
            public void onSuccessgetRequest(ArrayList<NotificationNetwork> listNotificationNetwork) {
                if(listNotificationNetwork.isEmpty()) {
                    view.onNotificationsEmpty();
                }else{
                    view.onGetNotificationsLocal(getNotificationLocal(listNotificationNetwork, VISTAS), getNotificationLocal(listNotificationNetwork, NO_VISTAS));
                }
            }

            @Override
            public void onError() {
                view.onError("fallo al consultar FS");
            }
        });
    }

    private ArrayList<NotificationLocal> getNotificationLocal(ArrayList<NotificationNetwork> listNotificationNetwork, String vistas) {
        //convert NotificationNetwork to NotificationLocal
        ArrayList<NotificationLocal> notificationLocalsVistas = new ArrayList<>();
        ArrayList<NotificationLocal> notificationLocalsnoVistas = new ArrayList<>();

        for (NotificationNetwork notificationNetwork : listNotificationNetwork) {
            NotificationLocal notificationLocal = new NotificationLocal();
            notificationLocal.setId(notificationNetwork.getId());
            notificationLocal.setMonto(String.valueOf(notificationNetwork.getMonto()));
            notificationLocal.setStatus(notificationNetwork.getStatus());
            notificationLocal.setRef(notificationNetwork.getReferencia());
            notificationLocal.setPayee(notificationNetwork.getNombrePayee());
            notificationLocal.setIdPayee(notificationNetwork.getIdPayee());
            notificationLocal.setIdOrder(notificationNetwork.getOrden());
            SimpleDateFormat sfd = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            notificationLocal.setDate(sfd.format(notificationNetwork.getFecha()));
            notificationLocal.setSee(notificationNetwork.isVista());

            if (notificationNetwork.isVista()) {
                notificationLocalsnoVistas.add(notificationLocal);
            } else {
                notificationLocalsVistas.add(notificationLocal);
            }

        }

        if (vistas.equals(VISTAS))
            return notificationLocalsVistas;
        else
            return notificationLocalsnoVistas;
    }

    public interface View {
        void onGetNotificationsLocal(ArrayList<NotificationLocal> notifyView, ArrayList<NotificationLocal> notifyNotView);

        void onError(String msg);

        void onNotificationsEmpty();

    }

}
