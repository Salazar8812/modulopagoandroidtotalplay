package com.totalplay.modulepayandroid.modules.notifications.presenters;

import androidx.appcompat.app.AppCompatActivity;

import com.totalplay.modulepayandroid.common.model.BaseEventCallback;
import com.totalplay.modulepayandroid.firebase.login.FireStore;

public class UpdateNotificationPresenter {

    private AppCompatActivity appCompatActivity;
    private FireStore fireStore;

    public UpdateNotificationPresenter(AppCompatActivity appCompatActivity) {
        this.appCompatActivity = appCompatActivity;
        this.fireStore = new FireStore();
    }

    public void updateViewNotifiy(String docId, UpdateNotifyView view){
        fireStore.updateViewNotification(docId , new BaseEventCallback() {
            @Override
            public void onSuccess() {
                view.onSucess();
            }

            @Override
            public void onError() {
                view.onError();
            }
        });
    }

    public interface UpdateNotifyView{
        void onSucess();
        void onError();
    }

}
