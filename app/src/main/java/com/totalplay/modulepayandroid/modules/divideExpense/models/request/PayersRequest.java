package com.totalplay.modulepayandroid.modules.divideExpense.models.request;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class PayersRequest {

	@SerializedName("payee")
	private String payee;

	@SerializedName("payers")
	private List<PayersItem> payers;

	@SerializedName("reference")
	private String reference;

	@SerializedName("amount")
	private double amount;

	@SerializedName("order")
	private String order;

	public void setPayee(String payee){
		this.payee = payee;
	}

	public String getPayee(){
		return payee;
	}

	public void setPayers(List<PayersItem> payers){
		this.payers = payers;
	}

	public List<PayersItem> getPayers(){
		return payers;
	}

	public void setReference(String reference){
		this.reference = reference;
	}

	public String getReference(){
		return reference;
	}

	public void setAmount(double amount){
		this.amount = amount;
	}

	public double getAmount(){
		return amount;
	}

	public void setOrder(String order){
		this.order = order;
	}

	public String getOrder(){
		return order;
	}
}