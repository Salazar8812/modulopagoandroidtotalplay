package com.totalplay.modulepayandroid.modules.notifications.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.totalplay.modulepayandroid.MainAppCompatActivity;
import com.totalplay.modulepayandroid.R;
import com.totalplay.modulepayandroid.modules.main.view.activities.MyAccountNewActivity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PayPayeeActivity extends MainAppCompatActivity {

    public static final String QUANTITY = "mQuantity";
    public static final String FRIEND = "mFriend";
    public static final String DATE = "date";
    public static final String REFERENCE = "reference";
    public static final String ORDER = "order";

    String mPayee;
    String mMonto;
    String mDate;
    String mRef;
    String mOrder;

    @BindView(R.id.mAmmountTextView)
    TextView tvAmount;

    @BindView(R.id.mNameToTransferTextView)
    TextView tvName;

    @BindView(R.id.mFechaTransferencia)
    TextView mFechaTransferencia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.activity_pay_payee);
        ButterKnife.bind(this);
        getIntents();
        setData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        onSetActivity(this);
        onSetTitle("PAGO EXITOSO");
    }


    private void setData() {
        tvAmount.setText("$" + mMonto);
        tvName.setText(mPayee);
        DateFormat df = new SimpleDateFormat("EEE, d MMM yyyy, HH:mm");
        String date = df.format(Calendar.getInstance().getTime());

        mFechaTransferencia.setText(date);
    }

/*
    private String getFormatDate(String mDate) {
        String day = mDate.substring(0, 2);
        String month = getMonth(mDate.substring(3, 5));
        String year = mDate.substring(6, 11);
        String hour = mDate.substring(11, 16);
        return day + "-" + month + "-" + year + "-" + hour + "h";
    }
*/

    private String getMonth(String monthNumber) {
        switch (monthNumber) {
            case "01":
                return "Enero";
            case "02":
                return "Febrero";
            case "03":
                return "Marzo";
            case "04":
                return "Abril";
            case "05":
                return "Mayo";
            case "06":
                return "Junio";
            case "07":
                return "Julio";
            case "08":
                return "Agosto";
            case "09":
                return "Septiembre";
            case "10":
                return "Octubre";
            case "11":
                return "Noviembre";
            case "12":
                return "Diciembre";
        }
        return "";
    }

    private void getIntents() {
        mMonto = getIntent().getStringExtra(QUANTITY);
        mPayee = getIntent().getStringExtra(FRIEND);
        mDate = getIntent().getStringExtra(DATE);
        mRef = getIntent().getStringExtra(REFERENCE);
        mOrder = getIntent().getStringExtra(ORDER);
    }

    @OnClick({R.id.mButtonCompartir, R.id.mButtonSalir})
    public void onClick(View view) {
        Intent newIntent = new Intent(this, MyAccountNewActivity.class);
        newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        switch (view.getId()) {
            case R.id.mButtonCompartir:
                startActivity(newIntent);
                break;
            case R.id.mButtonSalir:
                startActivity(newIntent);
                break;
        }
    }
}