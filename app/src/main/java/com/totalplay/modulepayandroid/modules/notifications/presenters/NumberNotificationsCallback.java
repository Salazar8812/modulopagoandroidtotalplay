package com.totalplay.modulepayandroid.modules.notifications.presenters;

public interface NumberNotificationsCallback {
    void onGetNumber(int number);
    void onError(String msg);
}
