package com.totalplay.modulepayandroid.modules.login.view;

public interface TokenView {
    void onSucessGetToken(String token);
    void onError(int resMsg);
}
