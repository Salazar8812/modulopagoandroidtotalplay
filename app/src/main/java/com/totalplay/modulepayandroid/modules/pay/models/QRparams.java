package com.totalplay.modulepayandroid.modules.pay.models;

public class QRparams {
    private String idUser;
    private String monto;
    private String fixedAmount;
    private String nOrder;
    private String typeUser;

    public QRparams() {
    }

    public QRparams(String idUser, String monto, String fixedAmount, String nOrder, String typeUser) {
        this.idUser = idUser;
        this.monto = monto;
        this.fixedAmount = fixedAmount;
        this.nOrder = nOrder;
        this.typeUser = typeUser;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }


    public String getnOrder() {
        return nOrder;
    }

    public void setnOrder(String nOrder) {
        this.nOrder = nOrder;
    }

    public String getFixedAmount() {
        return fixedAmount;
    }

    public void setFixedAmount(String fixedAmount) {
        this.fixedAmount = fixedAmount;
    }

    public String getTypeUser() {
        return typeUser;
    }

    public void setTypeUser(String typeUser) {
        this.typeUser = typeUser;
    }
}
