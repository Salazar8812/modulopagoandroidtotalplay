package com.totalplay.modulepayandroid.modules.main.presenters;

import androidx.appcompat.app.AppCompatActivity;

import com.resources.background.WSBaseResponseInterface;
import com.resources.background.WSCallback;
import com.resources.mvp.BasePresenter;
import com.resources.utils.MessageUtils;
import com.totalplay.modulepayandroid.R;
import com.totalplay.modulepayandroid.background.WSManagerNewEnvironment;
import com.totalplay.modulepayandroid.background.models.Response.balance.BalanceResponse;
import com.totalplay.modulepayandroid.background.webServices;

import java.util.regex.Pattern;

public class BalancePresenter extends BasePresenter implements WSCallback {

    private BalanceCallback balanceCallback;
    private AppCompatActivity mContext;

    public BalancePresenter(AppCompatActivity appCompatActivity, BalanceCallback balanceCallback) {
        super(appCompatActivity);
        this.balanceCallback = balanceCallback;
        this.mContext = appCompatActivity;

    }

    @Override
    public void onRequestWS(String requestUrl) {

    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        MessageUtils.stopProgress();
        if (requestUrl.equals(WSManagerNewEnvironment.WS.BALANCE)) {
            try {
                BalanceResponse balanceResponse = (BalanceResponse) baseResponse;
                balanceCallback.onGetDataUser(getCurrentBalance(balanceResponse.getBalance()));
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        } else if (requestUrl.equals(WSManagerNewEnvironment.WS.BALANCE_COMMERCE)) {
            try {
                BalanceResponse balanceResponse = (BalanceResponse) baseResponse;
                balanceCallback.onGetDataUser(getCurrentBalance(balanceResponse.getBalance()));
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        } else {
            balanceCallback.onErrorBalance(R.string.error_response_query_history);
        }
    }

    @Override
    public void onErrorLoadResponse(String requestUrl, String messageError) {
        MessageUtils.stopProgress();
        balanceCallback.onErrorBalance(R.string.error_response_query_balance);

    }

    @Override
    public void onErrorConnection() {

    }

    public void getBalance(String id) {
        MessageUtils.progress(mContext, "Cargando");
        WSManagerNewEnvironment.init().settings(mContext, this)
                .requestWs(BalanceResponse.class,
                        WSManagerNewEnvironment.WS.BALANCE, webServices.servicesAuth().getCustomerBalance(id));

    }

    private String getCurrentBalance(String currentBalance) {
        /*
        currentBalance = "123456789.90"; //123,456,789.90
        currentBalance = "23456789.90"; //23,456,789.90
        currentBalance = "3456789.90"; //3,456,789.90
        currentBalance = "456789.90"; //456,789.90
        currentBalance = "0.0"; //456,789.00
*/
        String balanceInt = getBalancePart(currentBalance, true);
        String balanceFloat = getBalancePart(currentBalance, false);
        String strInv = getStrInv(balanceInt);
        String strComas = getStrComas(strInv);
        String strReal = getStrInv(strComas) + "." + balanceFloat;
        if (strReal.substring(0, 1).equals(",")) {
            strReal = strReal.substring(1);
        }
        return "$" + strReal;
    }

    private String getStrComas(String strInv) {
        int j = 0;
        String strWithComas = "";
        for (int i = 0; i < strInv.length(); i++) {
            strWithComas = strWithComas + strInv.charAt(i);
            j++;
            if (j == 3) {
                j = 0;
                strWithComas = strWithComas + ",";
            }
        }
        return strWithComas;
    }

    private String getBalancePart(String currentBalance, boolean integer) {
        if (currentBalance.equals("0")) {
            return "0";
        }

        String separador = Pattern.quote(".");
        String[] parts = currentBalance.split(separador);
        if (parts.length == 2) {
            if (integer) {
                return parts[0];
            } else {
                return parts[1];
            }
        } else {
            if (integer) {
                return parts[0];
            } else {
                return "00";
            }

        }
    }

    private String getStrInv(String balanceInt) {
        String strInv = "";
        for (int x = balanceInt.length() - 1; x >= 0; x--)
            strInv = strInv + balanceInt.charAt(x);

        return strInv;
    }

    public interface BalanceCallback {
        void onGetDataUser( String currentBalance);

        void onErrorConnection();

        void onErrorBalance(int resMsg);

        void onLoadResponse();
    }
}
