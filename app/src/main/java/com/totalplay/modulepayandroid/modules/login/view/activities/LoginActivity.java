package com.totalplay.modulepayandroid.modules.login.view.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.google.firebase.iid.FirebaseInstanceId;
import com.resources.utils.validators.EditTextValidator;
import com.resources.utils.validators.FormValidator;
import com.resources.utils.validators.Regex;
import com.totalplay.modulepayandroid.MainAppCompatActivity;
import com.totalplay.modulepayandroid.R;
import com.totalplay.modulepayandroid.common.model.pojo.Facturacion;
import com.totalplay.modulepayandroid.common.model.pojo.User;
import com.totalplay.modulepayandroid.data.PreferenceHelper;
import com.totalplay.modulepayandroid.firebase.login.LoginView;
import com.totalplay.modulepayandroid.modules.login.presenter.LoginPresenter;
import com.totalplay.modulepayandroid.modules.login.view.AuthView;
import com.totalplay.modulepayandroid.modules.login.view.TokenAcessView;
import com.totalplay.modulepayandroid.modules.login.view.TokenView;
import com.totalplay.modulepayandroid.modules.login.view.loginFSView;
import com.totalplay.modulepayandroid.modules.login.view.updateFSView;
import com.totalplay.modulepayandroid.modules.main.view.activities.MyAccountNewActivity;
import com.totalplay.modulepayandroid.modules.pay.view.activities.OrderResumeActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends MainAppCompatActivity implements LoginView {

    @BindView(R.id.et_login_user)
    EditText etLoginUser;
    @BindView(R.id.et_login_pass)
    EditText etLoginPass;
    @BindView(R.id.imgBtnFingerprint)
    ImageButton imgBtnFingerprint;
    @BindView(R.id.progresss)
    ConstraintLayout progressBar;
    @BindView(R.id.pbText)
    TextView pbText;
    @BindView(R.id.btn_login_start_session)
    Button btnLoginStartSession;

    private LoginPresenter loginPresenter;
    private FormValidator mFormValidator;
    private String[] mParameterQR = {"", "", "", "", ""};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        PreferenceHelper.init(getApplicationContext());
        initPresenter();
        initData();
        addValidator();
        checkAuth();
        loginPresenter.validationBiometricExist();
    }

    private void initData() {
        pbText.setText("Iniciando sesión");
    }

    private void checkAuth() {
        if (PreferenceHelper.getBoolean(User.LOGGED)) {
            startNewActivity();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        dynamicLinks();
    }

    void dynamicLinks() {
        try {
            FirebaseDynamicLinks.getInstance()
                    .getDynamicLink(getIntent())
                    .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                        @Override
                        public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                            Uri deepLink = null;
                            if (pendingDynamicLinkData != null) {
                                deepLink = pendingDynamicLinkData.getLink();
                                if (deepLink != null) {
                                    mParameterQR[0] = deepLink.getQueryParameter("idPayee");
                                    mParameterQR[1] = deepLink.getQueryParameter("method");
                                    mParameterQR[2] = deepLink.getQueryParameter("amount");
                                    mParameterQR[3] = deepLink.getQueryParameter("fixedAmount");
                                    mParameterQR[4] = deepLink.getQueryParameter("order");
                                    catchQR();
                                } else {
                                    Toast.makeText(getApplicationContext(), "Error en deeplink", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    })
                    .addOnFailureListener(this, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.w("Failure", "getDynamicLink:onFailure", e);
                        }
                    });
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


    void addValidator() {
        mFormValidator = new FormValidator(this);
        mFormValidator.addValidator(new EditTextValidator(etLoginUser, Regex.NOT_EMPTY, R.string.default_error_msg));
        mFormValidator.addValidator(new EditTextValidator(etLoginPass, Regex.NOT_EMPTY, R.string.default_error_msg));
    }

    private void initPresenter() {
        loginPresenter = new LoginPresenter(this, this);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }


    void catchQR() {
        Intent mIntent = new Intent(this, OrderResumeActivity.class);
        mIntent.putExtra("ParameterQR", mParameterQR);
        mIntent.putExtra("dynamicLink", "true");
        startActivity(mIntent);
    }

    @OnClick({R.id.btn_login_create_account, R.id.imgBtnFingerprint, R.id.btn_login_start_session})
    void OnClick(View view) {
        switch (view.getId()) {
            case R.id.btn_login_create_account:
//                startActivity(new Intent(this, CreateAccountActivity.class));
                break;
            case R.id.imgBtnFingerprint:
                finger();
                break;
            case R.id.btn_login_start_session:
                validation();
                break;
            default:
                break;
        }
    }

    private void finger() {
        ImageButton biometricLoginButton = findViewById(R.id.imgBtnFingerprint);
        biometricLoginButton.setOnClickListener(view -> {
            loginPresenter.getBiometricPrompt().authenticate(loginPresenter.getPrompInfo());
        });
    }

    private void validation() {
        if (mFormValidator.isValid()) {
            enabledElements(false);
            loginPresenter.loginWithUserAndPass(getTxtFromTv(etLoginUser), getTxtFromTv(etLoginPass), new AuthView() {
                @Override
                public void onSucess(String uid) {
                    getDataUser(uid);
                }

                @Override
                public void onError(String resMsg) {
                    enabledElements(true);
                    Toast.makeText(LoginActivity.this, resMsg, Toast.LENGTH_SHORT).show();
                }
            });

/*            loginPresenter.registerWithUserAndPass(getTxtFromTv(etLoginUser), getTxtFromTv(etLoginPass), new AuthRegisterView() {
                @Override
                public void onSucessRegister(String uid) {
                    getDataUser(uid);
                }

                @Override
                public void onError(int msg) {
                    Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();
                }
            });*/
        }

    }


    private void enabledElements(boolean b) {
        imgBtnFingerprint.setEnabled(b);
        btnLoginStartSession.setEnabled(b);
        progressBar.setVisibility(b ? View.GONE : View.VISIBLE);
    }

    private void getDataUser(String uid) {
        if (mFormValidator.isValid()) {
            loginPresenter.getDataUser(uid, new loginFSView() {
                @Override
                public void onSucess(User user) {
                    if (saveDataUser(user, uid)) {
                        getTokenKnow(new TokenView() {
                            @Override
                            public void onSucessGetToken(String token) {
                                PreferenceHelper.saveString(User.TKDISPOSITIVO, token);
                                updateToken2();
                            }

                            @Override
                            public void onError(int resMsg) {
                                Toast.makeText(LoginActivity.this, resMsg, Toast.LENGTH_SHORT).show();
                                enabledElements(true);
                            }
                        });
                    } else {
                        Toast.makeText(LoginActivity.this, "error al obtener informacion del usuario", Toast.LENGTH_SHORT).show();
                        enabledElements(true);
                    }
                }

                @Override
                public void onError(int msg) {
                    Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();
                    enabledElements(true);
                }
            });
        }
    }

    private void getTokenKnow(TokenView tokenView) {
        FirebaseInstanceId.getInstance()
                .getInstanceId()
                .addOnSuccessListener(mAppCompatActivity, instanceIdResult -> {
                    tokenView.onSucessGetToken(instanceIdResult.getToken());
                })
                .addOnFailureListener(mAppCompatActivity, e -> tokenView.onError(R.string.error_token));

    }

    private void updateToken2() {
        loginPresenter.updateToken2(new updateFSView() {
            @Override
            public void onUpdateToken() {
                startNewActivity();
            }

            @Override
            public void onErrorUpdateToken(int msg) {
                enabledElements(true);
                Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void startNewActivity() {
        startActivity(new Intent(this, MyAccountNewActivity.class));
    }

    private boolean saveDataUser(User user, String uid) {
        try {
            PreferenceHelper.clearData();
            PreferenceHelper.saveString(User.APELLIDO1, user.getApellido1());
            PreferenceHelper.saveString(User.APELLIDO2, user.getApellido2());
            PreferenceHelper.saveString(User.EMAIL, user.getEmail());
            PreferenceHelper.saveString(User.IDHUAWEI, user.getIdHuawei());
            PreferenceHelper.saveString(User.IDMAMBU, user.getIdMambu());
            PreferenceHelper.saveString(User.NOMBRE, user.getNombre());
            PreferenceHelper.saveString(User.UID, uid);
            loginPresenter.getTokenAccess(new TokenAcessView() {
                @Override
                public void onSuccess(String tk) {
                    PreferenceHelper.saveString(User.TKA, tk);
                }

                @Override
                public void onError(int resMsg) {
                    PreferenceHelper.saveString(User.TKA, null);
                }
            });

            if (PreferenceHelper.getString(User.TKA) == null) {
                return false;
            }

            if (user.getTipo().equals("comercio")) {
                PreferenceHelper.saveString(User.CATEGORIA, user.getCategoria());
            } else {
                PreferenceHelper.saveString(User.EMAIL, user.getEmail());
            }

            if (user.getFacturacion() != null) {
                PreferenceHelper.saveString(Facturacion.RAZON_SOCIAL, user.getFacturacion().getRazonSocial());
                PreferenceHelper.saveString(Facturacion.RFC, user.getFacturacion().getRfc());
                PreferenceHelper.saveString(Facturacion.TIPO_PERSONA, user.getFacturacion().getTipoPersona());
                PreferenceHelper.saveString(Facturacion.USO_CFDI, user.getFacturacion().getUsoCFDI());
            }
            return true;
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            enabledElements(true);
            return false;
        }
    }

    private String getTxtFromTv(TextView textView) {
        return textView.getText().toString().trim();
    }


    /*
     * check enabled biometric
     *
     * */
    @Override
    public void onBiometricEnabled() {
        imgBtnFingerprint.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBiometricDisabled(String msg) {
        imgBtnFingerprint.setVisibility(View.GONE);
    }

    /*
     * check authentication biometric
     * */

    @Override
    public void onAuthenticationError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAuthenticationSucceeded() {
        startActivity(new Intent(this, MyAccountNewActivity.class));
    }

    @Override
    public void onAuthenticationFailed(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void openUILogin() {

    }

    @Override
    public void onErrorFireStore() {
        enabledElements(true);
        Toast.makeText(this, "Error en fireStore", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onErrorPass(String error) {
        enabledElements(true);
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void openMainActivity(String idHuawei) {
        if (idHuawei.isEmpty()) {
            Toast.makeText(this, "idHuawei vacio", Toast.LENGTH_SHORT).show();
        } else {
            //Constantes.mIdPayer = idHuawei;
            enabledElements(true);
            startActivity(new Intent(this, MyAccountNewActivity.class));
//            startActivity(new Intent(this, MainActivity.class));
        }
    }


}
