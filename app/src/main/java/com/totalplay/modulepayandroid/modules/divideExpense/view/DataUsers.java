package com.totalplay.modulepayandroid.modules.divideExpense.view;

import com.totalplay.modulepayandroid.modules.divideExpense.models.Contact;

import java.util.ArrayList;

public interface DataUsers {
    void onDataUsers(ArrayList<Contact> contacts, String fragment);
}
