package com.totalplay.modulepayandroid.modules.login.view;

import com.totalplay.modulepayandroid.common.model.BaseEventCallback;

public interface AuthView {
    void onSucess(String uid);
    void onError(String resMsg);
}
