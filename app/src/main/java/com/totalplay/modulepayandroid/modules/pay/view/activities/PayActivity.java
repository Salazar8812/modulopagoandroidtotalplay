package com.totalplay.modulepayandroid.modules.pay.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.resources.mvp.BasePresenter;
import com.resources.utils.MessageUtils;
import com.resources.utils.validators.EditTextValidator;
import com.resources.utils.validators.FormValidator;
import com.resources.utils.validators.Regex;
import com.totalplay.modulepayandroid.MainAppCompatActivity;
import com.totalplay.modulepayandroid.R;
import com.totalplay.modulepayandroid.Utils.BiometricView;
import com.totalplay.modulepayandroid.Utils.OperationsUtils;
import com.totalplay.modulepayandroid.common.Constants;
import com.totalplay.modulepayandroid.common.view.dialogs.ConfirmPasswordDialog;
import com.totalplay.modulepayandroid.firebase.BiometricPresenter;
import com.totalplay.modulepayandroid.modules.main.view.activities.MyAccountNewActivity;
import com.totalplay.modulepayandroid.modules.pay.models.QRparams;
import com.totalplay.modulepayandroid.modules.pay.presenters.PayPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PayActivity extends MainAppCompatActivity implements BiometricView, PayPresenter.View {

    public static final String ID_USER = "id_user";
    public static final String MONTO = "monto";
    public static final String FIXED_AMOUNT = "fixed_amount";
    public static final String ORDER = "order";
    public static final String TYPE_USER = "type_user";

    PayPresenter mCreatePayOrderPresenter;

    FormValidator mValidator;
    @BindView(R.id.tv_other_cantidad_pay)
    TextView tvOtherCantidadPay;
    @BindView(R.id.tv_write_cantidad_pay)
    TextView tvWriteCantidadPay;
    @BindView(R.id.tv_pesos_pay)
    TextView tvPesosPay;
    @BindView(R.id.rg_propina_pay)
    RadioGroup rgPropinaPay;
    @BindView(R.id.btn_10_percent_pro_pay)
    RadioButton btn10PercentProPay;
    @BindView(R.id.btn_15_percent_pro_pay)
    RadioButton btn15PercentProPay;
    @BindView(R.id.btn_20_percent_pro_pay)
    RadioButton btn20PercentProPay;

    @BindView(R.id.iv_amvorguesa_pay)
    ImageView ivAmvorguesaPay;
    @BindView(R.id.tv_resume_order_pay)
    TextView tvResumeOrderPay;
    @BindView(R.id.v_resume_view1_pay)
    View vResumeView1Pay;
    @BindView(R.id.tv_propina_pay)
    TextView tvPropinaPay;
    @BindView(R.id.tv_total_pay)
    TextView tvTotalPay;

    @BindView(R.id.constraint_flink)
    ConstraintLayout constraintFlink;
    @BindView(R.id.constraint_iptv)
    ConstraintLayout constraintIptv;
    @BindView(R.id.constraint_other_user)
    ConstraintLayout constraintOtherUser;

    @BindView(R.id.mInputAmmount_users)
    EditText mInputAmmountUsers;
    @BindView(R.id.mInputAmmount)
    EditText mInputAmmount;
    @BindView(R.id.mInputAmmount_iptv)
    EditText mInputAmmountIptv;
    //propina
    @BindView(R.id.mInputAmmount2)
    EditText mInputAmmount2;
    @BindView(R.id.tv_propina_pay_total)
    TextView tvPropinaPayTotal;
    //checkbox facturacion
    @BindView(R.id.cb_flink_fatura)
    CheckBox cbFlinkFatura;

    @BindView(R.id.cb_iptv_fatura)
    CheckBox cbIptvFatura;

    @BindView(R.id.tv_flink_facturar_pay)
    TextView tvFlinkFacturarPay;

    @BindView(R.id.cl_pb_loading)
    ConstraintLayout clPbLoading;

    @BindView(R.id.sv_content)
    ScrollView svContent;

    private BiometricPresenter biometricPresenter;
    private QRparams qRparams;

    private boolean cb10var = true;
    private boolean cb15var = true;
    private boolean cb20var = true;

    //banderas para propina
    private static final int CB_PROPINA = 350490871;
    private static final int ET_PROPINA = 450490871;

    private int flagPropina = 0;
    TextWatcher alternateTextWatcher;
    private String mUserPay = "";
    private boolean mAcceptFacturaBusiness = false;
    private boolean mAcceptFacturaUser = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.activity_pay);
        ButterKnife.bind(this);
        qRparams = getQRparams();
        addValidator();
        configPay();
        showProgress();
        mCreatePayOrderPresenter.userFactura();
        mCreatePayOrderPresenter.getDataPay(qRparams.getIdUser());
        initChecks();
        textWatcher();
    }

    private void textWatcher() {
        alternateTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setData(OperationsUtils.getCurrentBalance(editable.toString()));
            }
        };
        mInputAmmountUsers.addTextChangedListener(alternateTextWatcher);

    }

    private QRparams getQRparams() {
        QRparams qRparams = new QRparams();
        qRparams.setIdUser(getIntent().getStringExtra(ID_USER));
        qRparams.setMonto(getIntent().getStringExtra(MONTO));
        qRparams.setFixedAmount(getIntent().getStringExtra(FIXED_AMOUNT));
        qRparams.setnOrder(getIntent().getStringExtra(ORDER));
        qRparams.setTypeUser(getIntent().getStringExtra(TYPE_USER));
        return qRparams;
    }

    private void setData(String text) {
        mInputAmmountUsers.removeTextChangedListener(alternateTextWatcher);
        mInputAmmountUsers.setText(text);
        mInputAmmountUsers.setSelection(text.length() - 3);
        mInputAmmountUsers.addTextChangedListener(alternateTextWatcher);
    }

    private void initChecks() {
        btn10PercentProPay.setBackground(getResources().getDrawable(R.drawable.shape));
        btn15PercentProPay.setBackground(getResources().getDrawable(R.drawable.shape));
        btn20PercentProPay.setBackground(getResources().getDrawable(R.drawable.shape));
    }

    void addValidator() {
        mValidator = new FormValidator(this);
    }

    @Override
    protected BasePresenter[] getPresenters() {
        BasePresenter[] basePresenters = new BasePresenter[2];
        biometricPresenter = new BiometricPresenter(this, this);
        mCreatePayOrderPresenter = new PayPresenter(this, this);

        basePresenters[0] = biometricPresenter;
        basePresenters[1] = mCreatePayOrderPresenter;
        return basePresenters;
    }

    private void configPay() {
        checkLayout(qRparams.getTypeUser());
        if (qRparams.getFixedAmount().equals("true")) {
            setTxtEnabled(mInputAmmount, true);
            setTxtEnabled(mInputAmmountIptv, true);
            setTxtEnabled(mInputAmmountUsers, true);
            /*editext enabled*/
        } else {
            setTxtEnabled(mInputAmmount, false);
            setTxtEnabled(mInputAmmountIptv, false);
            setTxtEnabled(mInputAmmountUsers, false);
        }
    }

    private void setTxtEnabled(EditText et, boolean b) {
        if (b) {
            et.setText(getTextCompose());
            et.setEnabled(false);
        } else {
            et.setEnabled(true);
        }
    }

    private String getTextCompose() {
        return "$" + OperationsUtils.getCurrentBalance(qRparams.getMonto());
    }

    private void checkLayout(String s) {
        switch (s) {
            case Constants.IPTV:
                constraintIptv.setVisibility(View.VISIBLE);
                constraintFlink.setVisibility(View.GONE);
                constraintOtherUser.setVisibility(View.GONE);
                break;
            case Constants.FLINK:
                constraintFlink.setVisibility(View.VISIBLE);
                constraintIptv.setVisibility(View.GONE);
                constraintOtherUser.setVisibility(View.GONE);
                break;
            case Constants.USERS:
                mValidator.addValidator(new EditTextValidator(mInputAmmountUsers, Regex.DECIMAL, R.string.errorfields));
                constraintOtherUser.setVisibility(View.VISIBLE);
                constraintIptv.setVisibility(View.GONE);
                constraintFlink.setVisibility(View.GONE);
                break;
        }
    }

    void selectTypeTransder() {
        String propina = "0.00";
        String montoSinComas;
        String propinaSinComas = "0.00";

        switch (qRparams.getTypeUser()) {
            case Constants.USERS:
                montoSinComas = OperationsUtils.getStrSinComas(mInputAmmountUsers.getText().toString().trim());
                mCreatePayOrderPresenter.CreatePayOrdersP2P(montoSinComas, qRparams.getIdUser(), qRparams.getnOrder() );
                break;
            case Constants.FLINK:
                if (flagPropina != 0) {
                    if (flagPropina == CB_PROPINA) {
                        propina = tvPropinaPayTotal.getText().toString().trim();
                        propinaSinComas = OperationsUtils.getStrSinComas(propina.substring(1));
                    } else {
                        propina = OperationsUtils.geStrTwoDecimals(mInputAmmount2.getText().toString().trim());
                        propinaSinComas = OperationsUtils.getStrSinComas(propina);
                    }
                }
                montoSinComas = OperationsUtils.getStrSinComas(mInputAmmount.getText().toString().substring(1).trim());
                mCreatePayOrderPresenter.CreatePayOrderC2B(montoSinComas, qRparams.getIdUser(), qRparams.getnOrder(), propinaSinComas, cbFlinkFatura.isChecked());
                break;
            case Constants.IPTV:
                montoSinComas = OperationsUtils.getStrSinComas(mInputAmmountIptv.getText().toString().substring(1).trim());
                mCreatePayOrderPresenter.CreatePayOrderC2B(montoSinComas, qRparams.getIdUser(), qRparams.getnOrder(), propina, cbIptvFatura.isChecked());
                break;
            default:
                break;
        }
    }

    private boolean getStatusProd() {
        return false;
    }


    @Override
    protected void onResume() {
        super.onResume();
        onSetActivity(this);
        onSetTitle("PAGAR");
    }

    @OnClick({R.id.mPayButton, R.id.mExitButton})
    void OnClick(View view) {
        switch (view.getId()) {
            case R.id.mPayButton:
                if (!qRparams.getTypeUser().equals(Constants.FLINK) && !qRparams.getTypeUser().equals(Constants.IPTV)) {
                    if (mValidator.isValid()) {
                        biometricPresenter.validationBiometricExist();
                    }
                } else {
                    biometricPresenter.validationBiometricExist();
                }
                break;

            case R.id.mExitButton:
                this.finish();
                break;
            default:
                break;
        }
    }

    @Override
    public void onBiometricEnabled() {
        biometricPresenter.getBiometricPrompt().authenticate(biometricPresenter.getPrompInfo());
    }

    @Override
    public void onBiometricDisabled(String msg) {
        //selectTypeTransder(mIdPayee);
        ConfirmPasswordDialog confirmPasswordDialog = ConfirmPasswordDialog.newInstance();
        confirmPasswordDialog.getAuthCredentials(new payCredentials() {

            @Override
            public void onSuccess() {
                selectTypeTransder();
            }

            @Override
            public void onFailure(String resMsg) {
                MessageUtils.toast(getApplicationContext(), "Error de usuario o contraseña");
            }
        });
        confirmPasswordDialog.show(this.getSupportFragmentManager(), "confirmPassword");
        //startActivityForResult(new Intent(this, DialogValidateSession.class), 666);
    }

    @Override
    public void onAuthenticationError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onAuthenticationSucceeded() {
        selectTypeTransder();
    }

    @Override
    public void onAuthenticationFailed(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.tv_other_cantidad_pay)
    public void onClick() {
        invFunc(true);
        btn10PercentProPay.setBackground(getResources().getDrawable(R.drawable.shape));
        btn15PercentProPay.setBackground(getResources().getDrawable(R.drawable.shape));
        btn20PercentProPay.setBackground(getResources().getDrawable(R.drawable.shape));
        cb15var = true;
        cb20var = true;
        cb10var = true;

    }

    private void invFunc(boolean b) {
        if (b) {
            //propina desde el edittext
            flagPropina = ET_PROPINA;
            tvOtherCantidadPay.setVisibility(View.GONE);
            tvPropinaPayTotal.setVisibility(View.GONE);
            tvWriteCantidadPay.setVisibility(View.VISIBLE);
            tvPesosPay.setVisibility(View.VISIBLE);
            mInputAmmount2.setVisibility(View.VISIBLE);
        } else {
            flagPropina = CB_PROPINA;
            tvOtherCantidadPay.setVisibility(View.VISIBLE);
            tvPropinaPayTotal.setVisibility(View.VISIBLE);
            tvWriteCantidadPay.setVisibility(View.GONE);
            tvPesosPay.setVisibility(View.GONE);
            mInputAmmount2.setVisibility(View.GONE);
        }
    }

    @OnClick({R.id.btn_10_percent_pro_pay, R.id.btn_15_percent_pro_pay, R.id.btn_20_percent_pro_pay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_10_percent_pro_pay:
                btn10PercentProPay.setChecked(cb10var);
                invFunc(false);
                if (cb10var) {
                    btn10PercentProPay.setBackground(getResources().getDrawable(R.drawable.shape2));
                    tvPropinaPayTotal.setVisibility(View.VISIBLE);
                    tvPropinaPayTotal.setText(OperationsUtils.getPropina(mInputAmmount.getText().toString(), 10));
                } else {
                    btn10PercentProPay.setBackground(getResources().getDrawable(R.drawable.shape));
                    tvPropinaPayTotal.setVisibility(View.GONE);
                }
                btn15PercentProPay.setBackground(getResources().getDrawable(R.drawable.shape));
                btn20PercentProPay.setBackground(getResources().getDrawable(R.drawable.shape));

                cb10var = !cb10var;
                cb15var = true;
                cb20var = true;

                break;
            case R.id.btn_15_percent_pro_pay:
                btn15PercentProPay.setChecked(cb15var);
                invFunc(false);
                if (cb15var) {
                    btn15PercentProPay.setBackground(getResources().getDrawable(R.drawable.shape2));
                    tvPropinaPayTotal.setVisibility(View.VISIBLE);
                    tvPropinaPayTotal.setText(OperationsUtils.getPropina(mInputAmmount.getText().toString(), 15));
                } else {
                    btn15PercentProPay.setBackground(getResources().getDrawable(R.drawable.shape));
                    tvPropinaPayTotal.setVisibility(View.GONE);
                }
                btn10PercentProPay.setBackground(getResources().getDrawable(R.drawable.shape));
                btn20PercentProPay.setBackground(getResources().getDrawable(R.drawable.shape));
                cb10var = true;
                cb20var = true;
                cb15var = !cb15var;
                break;
            case R.id.btn_20_percent_pro_pay:
                btn20PercentProPay.setChecked(cb20var);
                invFunc(false);
                if (cb20var) {
                    btn20PercentProPay.setBackground(getResources().getDrawable(R.drawable.shape2));
                    tvPropinaPayTotal.setVisibility(View.VISIBLE);
                    tvPropinaPayTotal.setText(OperationsUtils.getPropina(mInputAmmount.getText().toString(), 20));
                } else {
                    btn20PercentProPay.setBackground(getResources().getDrawable(R.drawable.shape));
                    tvPropinaPayTotal.setVisibility(View.GONE);
                }
                btn15PercentProPay.setBackground(getResources().getDrawable(R.drawable.shape));
                btn10PercentProPay.setBackground(getResources().getDrawable(R.drawable.shape));
                cb20var = !cb20var;
                cb15var = true;
                cb10var = true;
                break;
        }
    }

    /*datos que vienen de FireStore*/

    @Override
    public void onSuccessUserFactura(boolean b) {
        mAcceptFacturaUser = b;
    }

    @Override
    public void onErrorFactura() {
        Toast.makeText(this, "Error al consultar datos factura", Toast.LENGTH_SHORT).show();
    }

    /*datos que vienen de service*/
    @Override
    public void onSuccessBusinesstData(String user, boolean isFatura) {
        mUserPay = user;
        mAcceptFacturaBusiness = isFatura;
    }

    @Override
    public void onErrorConnection() {
        Toast.makeText(this, "Error de conexión", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void goneProgress() {
        if (mAcceptFacturaUser && mAcceptFacturaBusiness) {
            cbFlinkFatura.setVisibility(View.VISIBLE);
            tvFlinkFacturarPay.setVisibility(View.VISIBLE);
        } else {
            cbFlinkFatura.setVisibility(View.GONE);
            tvFlinkFacturarPay.setVisibility(View.GONE);
        }

        svContent.setVisibility(View.VISIBLE);
        clPbLoading.setVisibility(View.GONE);
    }

    private void showProgress() {
        svContent.setVisibility(View.GONE);
        clPbLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void onErrorGetData(int resMsg) {
        Toast.makeText(this, resMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSucessPay(String mMonto, String mPayee, String mReference, String typeTransaction) {
        Intent mIntent1 = new Intent(this, PaySuccessActivity.class);
        mIntent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        mIntent1.putExtra(PaySuccessActivity.MONTO, mMonto);
        mIntent1.putExtra(PaySuccessActivity.PAYEE, mPayee);
        mIntent1.putExtra(PaySuccessActivity.REFERENCE, mReference);
        mIntent1.putExtra(PaySuccessActivity.USER_PAY, mUserPay);
        mIntent1.putExtra(PaySuccessActivity.TYPE_TRANSACTION, typeTransaction);
        startActivity(mIntent1);
    }

    @Override
    public void onSuccessPayTip(String mMonto, String mPayee, String mReference, String mPropina, String mOrder, String typeTransaction) {
        Intent mIntent4 = new Intent(this, PaySuccessActivity.class);
        mIntent4.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        mIntent4.putExtra(PaySuccessActivity.MONTO, mMonto);
        mIntent4.putExtra(PaySuccessActivity.PAYEE, mPayee);
        mIntent4.putExtra(PaySuccessActivity.REFERENCE, mReference);
        mIntent4.putExtra(PaySuccessActivity.TIP, mPropina);
        mIntent4.putExtra(PaySuccessActivity.ORDER, mOrder);
        mIntent4.putExtra(PaySuccessActivity.USER_PAY, mUserPay);
        mIntent4.putExtra(PaySuccessActivity.TYPE_TRANSACTION, typeTransaction);
        startActivity(mIntent4);
    }

    @Override
    public void onErrorPay(int resMsg) {
        Toast.makeText(this, resMsg, Toast.LENGTH_SHORT).show();
    }


    public interface payCredentials {
        void onSuccess();

        void onFailure(String resMsg);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent newIntent = new Intent(this, MyAccountNewActivity.class);
        newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(newIntent);

    }
}


