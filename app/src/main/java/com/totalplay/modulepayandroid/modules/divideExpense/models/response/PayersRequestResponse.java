package com.totalplay.modulepayandroid.modules.divideExpense.models.response;

import com.google.gson.annotations.SerializedName;
import com.resources.background.WSBaseResponse;

public class PayersRequestResponse  extends WSBaseResponse {

	@SerializedName("message")
	private String message;

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}
}