package com.totalplay.modulepayandroid.modules.divideExpense.view.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewpager.widget.PagerAdapter;

import com.totalplay.modulepayandroid.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BalanceAdapter extends PagerAdapter {

    private LayoutInflater layoutInflater;
    private Context context;
    private String balance;

    @BindView(R.id.mLayoutAdd)
    LinearLayout mLayoutAdd;

    @BindView(R.id.mLayoutBalance)
    ConstraintLayout mLayoutBalance;

    @BindView(R.id.mTvBalance)
    TextView mTvBalance;

    public BalanceAdapter( Context context, String balance) {
        this.context = context;
        this.balance = balance;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.item_viewpager, container, false);
        ButterKnife.bind(this,view);

        if(position == 0){
            mLayoutBalance.setVisibility(View.VISIBLE);
            mLayoutAdd.setVisibility(View.GONE);
            mTvBalance.setText(balance);
        }else {
            mLayoutBalance.setVisibility(View.GONE);
            mLayoutAdd.setVisibility(View.VISIBLE);
        }

        container.addView(view, 0);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);
    }
}