package com.totalplay.modulepayandroid.modules.login.presenter;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.biometric.BiometricManager;
import androidx.biometric.BiometricPrompt;
import androidx.core.content.ContextCompat;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;
import com.resources.mvp.BasePresenter;
import com.totalplay.modulepayandroid.R;
import com.totalplay.modulepayandroid.firebase.Authentication.model.dataAccess.Authentication;
import com.totalplay.modulepayandroid.common.model.BaseEventCallback;
import com.totalplay.modulepayandroid.common.model.GetUserCallback;
import com.totalplay.modulepayandroid.common.model.pojo.User;
import com.totalplay.modulepayandroid.firebase.Authentication.model.dataAccess.StatusAuthCallback;
import com.totalplay.modulepayandroid.firebase.Authentication.model.dataAccess.StatusAuthCreCallback;
import com.totalplay.modulepayandroid.firebase.Authentication.model.dataAccess.StatusAuthRegisterCallback;
import com.totalplay.modulepayandroid.firebase.Authentication.model.dataAccess.TokenCallback;
import com.totalplay.modulepayandroid.firebase.login.FireStore;
import com.totalplay.modulepayandroid.firebase.login.LoginView;
import com.totalplay.modulepayandroid.modules.login.view.AuthRegisterView;
import com.totalplay.modulepayandroid.modules.login.view.AuthView;
import com.totalplay.modulepayandroid.modules.login.view.TokenAcessView;
import com.totalplay.modulepayandroid.modules.login.view.TokenView;
import com.totalplay.modulepayandroid.modules.login.view.loginFSView;
import com.totalplay.modulepayandroid.modules.login.view.updateFSView;

import java.util.concurrent.Executor;


public class LoginPresenter {

    private AppCompatActivity mAppcompactActivity;
    private LoginView loginView;
    private Authentication mAuthentication;
    private FireStore mFireStore;
    private String tkDispositivo = "";


    public LoginPresenter(AppCompatActivity appCompatActivity, LoginView loginView) {
        this.mAppcompactActivity = appCompatActivity;
        this.loginView = loginView;
        this.mAuthentication = new Authentication();
        this.mFireStore = new FireStore();
    }


    public void loginWithUserAndPass(String user, String pass, AuthView authView) {
        mAuthentication.getAuthWithcredenciales(user, pass, new StatusAuthCreCallback() {
            @Override
            public void onGetUser(FirebaseUser user) {
                authView.onSucess(user.getUid());
            }

            @Override
            public void onError(String resMsg) {
                authView.onError(resMsg);
            }
        });
    }

    public void getTokenAccess(TokenAcessView tokenAcessView){
        mAuthentication.getToken(new TokenCallback() {
            @Override
            public void onSucessGetToken(String token) {
                tokenAcessView.onSuccess(token);
            }

            @Override
            public void onError(int resMsg) {
                tokenAcessView.onError(resMsg);
            }
        });
    }

    public void registerWithUserAndPass(String user, String pass, AuthRegisterView authRegisterView){
        mAuthentication.register(user, pass, new StatusAuthRegisterCallback() {
            @Override
            public void onGetUser(FirebaseUser user) {
                authRegisterView.onSucessRegister(user.getUid());
            }

            @Override
            public void onError() {
                authRegisterView.onError(R.string.error_register);
            }
        });
    }



    public void validationBiometricExist() {
        BiometricManager biometricManager = BiometricManager.from(mAppcompactActivity);
        switch (biometricManager.canAuthenticate()) {
            case BiometricManager.BIOMETRIC_SUCCESS:
                loginView.onBiometricEnabled();
                break;
            case BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE:
                loginView.onBiometricDisabled("No hay características biométricas disponibles en este dispositivo.");
                break;
            case BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE:
                loginView.onBiometricDisabled("Las características biométricas no están disponibles actualmente");
                break;
            case BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED:
                loginView.onBiometricDisabled("El usuario no ha asociado ninguna credencial biométrica con su cuenta.");
                break;
        }
    }

    public BiometricPrompt getBiometricPrompt() {
        Executor executor = ContextCompat.getMainExecutor(mAppcompactActivity);
        return new BiometricPrompt(mAppcompactActivity,
                executor, new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode, @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                loginView.onAuthenticationError("Error al realizar la autentificación");
            }

            @Override
            public void onAuthenticationSucceeded(@NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                loginView.onAuthenticationSucceeded();
            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
                loginView.onAuthenticationFailed("el dedo se movió demasiado rápido !");

            }
        });
    }

    public BiometricPrompt.PromptInfo getPrompInfo() {
        return new BiometricPrompt.PromptInfo.Builder()
                .setTitle("Autentificación biometrica")
                .setSubtitle("")
                .setNegativeButtonText("Cancelar")
                .build();
    }


    public void getDataUser(String uid, loginFSView loginFSView) {
        if(uid.isEmpty()){
            loginView.onErrorPass("usuario o constraseña incorrecto");
        }else{
            mFireStore.getUser(uid, new GetUserCallback() {
                @Override
                public void onSuccess(User userFB) {
                    loginFSView.onSucess(userFB);
                }

                @Override
                public void onError(String e) {
                    loginFSView.onError(R.string.error_user_firestore);
                }
            });
        }

    }

    public void updateToken2( updateFSView updateFSView) {
        mFireStore.updateToken(new BaseEventCallback() {
            @Override
            public void onSuccess() {
                updateFSView.onUpdateToken();
            }

            @Override
            public void onError() {
                updateFSView.onErrorUpdateToken(R.string.error_update_token);
            }
        });

    }


}
