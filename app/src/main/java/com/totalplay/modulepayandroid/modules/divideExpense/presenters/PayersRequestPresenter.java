package com.totalplay.modulepayandroid.modules.divideExpense.presenters;

import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.resources.background.WSBaseResponseInterface;
import com.resources.background.WSCallback;
import com.resources.mvp.BasePresenter;
import com.totalplay.modulepayandroid.background.WSManagerNewEnvironment;
import com.totalplay.modulepayandroid.background.webServices;
import com.totalplay.modulepayandroid.modules.divideExpense.models.request.PayersItem;
import com.totalplay.modulepayandroid.modules.divideExpense.models.request.PayersRequest;
import com.totalplay.modulepayandroid.modules.divideExpense.models.response.PayersRequestResponse;

import java.util.ArrayList;

public class PayersRequestPresenter extends BasePresenter implements WSCallback {

    private boolean DEBUG_LOCAL= false;

    private AppCompatActivity mAppcompactActivity;
    private PayersRequestPresenter.View view;

    public PayersRequestPresenter(AppCompatActivity mAppcompactActivity, View view) {
        super(mAppcompactActivity);
        this.mAppcompactActivity = mAppcompactActivity;
        this.view = view;
    }

    public void sendRequestToPayeers(ArrayList<PayersItem> payers, String payee, String ref, double amount, String order ){
        Log.d("REQUEST_LOG", "REQUEST : " + getJsonFromObject(getPayersRequest(payers, payee, ref, amount, order)));
        if(!DEBUG_LOCAL){
            WSManagerNewEnvironment.init().settings(mAppcompactActivity, this).
                    requestWs(
                            PayersRequestResponse.class,
                            WSManagerNewEnvironment.WS.CREATEPAYORDERP2P,
                            webServices.services().sendRequestPayers(getPayersRequest(payers, payee, ref, amount, order)));
        }else{
            evaluateResponse(new Gson().fromJson(WSManagerNewEnvironment.getJsonDebugUnit(WSManagerNewEnvironment.WS.REQUEST_PAYERS),  PayersRequestResponse.class));
        }
    }

    public static String getJsonFromObject(Object requestEncrypt) {
        Gson gson = new Gson();
        return gson.toJson(requestEncrypt);

    }

    private PayersRequest getPayersRequest(ArrayList<PayersItem> payers, String payee, String ref, double amount, String order) {
        PayersRequest payersRequest = new PayersRequest();
        payersRequest.setPayers(payers);
        payersRequest.setAmount(amount);
        payersRequest.setOrder(order);
        payersRequest.setReference(ref);
        payersRequest.setPayee(payee);
        return payersRequest;
    }

    @Override
    public void onRequestWS(String requestUrl) {

    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        evaluateResponse((PayersRequestResponse) baseResponse);
    }

    private void evaluateResponse(PayersRequestResponse payersRequestResponse) {
        view.onSuccessRequestSend(payersRequestResponse);
    }

    @Override
    public void onErrorLoadResponse(String requestUrl, String messageError) {
        view.onErrorRequestSend("Error en el servicio de solicitud de pago");
    }

    @Override
    public void onErrorConnection() {
        view.onErrorRequestSend("Error de conexión");
    }

    public interface View{
        void onSuccessRequestSend(PayersRequestResponse payersRequestResponse);
        void onErrorRequestSend(String msg);
    }

}
