package com.totalplay.modulepayandroid.modules.main.view.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.totalplay.modulepayandroid.R;
import com.totalplay.modulepayandroid.modules.main.models.Order;

import java.util.List;

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.ItemViewHolder>{

    private List<Order> list;

    public OrdersAdapter(List<Order> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movements, parent, false);
        return new ItemViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        final Order orderItem = list.get(position);
        holder.itemView.setOnClickListener(null);
        holder.tvNameBussiness.setText(orderItem.getBussiness());
        holder.tvPay.setText(orderItem.getPay());
        holder.tvDate.setText(orderItem.getDate());
/*
        holder.itemView.setTag(position);
        holder.itemView.setOnClickListener(v -> mListener.onItemClick(holder.itemView));
*/
    }

    public void updateList(List<Order> list) {
        this.list.clear();
        this.list.addAll(list);
        this.notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    static class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView tvNameBussiness;
        TextView tvPay;
        TextView tvDate;

        ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNameBussiness = itemView.findViewById(R.id.item_movements_bussines);
            tvPay = itemView.findViewById(R.id.item_movemments_pay);
            tvDate = itemView.findViewById(R.id.item_movements_date);
        }
    }

}
