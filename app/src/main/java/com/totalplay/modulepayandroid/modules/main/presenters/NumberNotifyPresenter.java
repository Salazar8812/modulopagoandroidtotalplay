package com.totalplay.modulepayandroid.modules.main.presenters;

import androidx.appcompat.app.AppCompatActivity;

import com.resources.mvp.BasePresenter;
import com.totalplay.modulepayandroid.firebase.login.FireStore;
import com.totalplay.modulepayandroid.modules.notifications.presenters.NumberNotificationsCallback;

public class NumberNotifyPresenter  extends BasePresenter {

    private FireStore fireStore;
    public NumberNotifyPresenter(AppCompatActivity appCompatActivity) {
        super(appCompatActivity);
        fireStore = new FireStore();
    }

    public void getNumberNotify(String uid, NumberNotificationsView view){
        fireStore.getNumberNotify(uid, new NumberNotificationsCallback(){

            @Override
            public void onGetNumber(int number) {
                if(number==0){
                    view.onSucessNumberZero();
                }else if(number > 9){
                    view.onSucessGetNumber("+9");
                }else{
                    view.onSucessGetNumber(String.valueOf(number));
                }
            }

            @Override
            public void onError(String msg) {
                view.onError();
            }
        });
    }

    public interface NumberNotificationsView {
        void onSucessGetNumber(String number);
        void onSucessNumberZero();
        void onError();
    }
}
