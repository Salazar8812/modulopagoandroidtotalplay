package com.totalplay.modulepayandroid.modules.main.presenters;

import androidx.appcompat.app.AppCompatActivity;

import com.resources.background.WSBaseResponseInterface;
import com.resources.background.WSCallback;
import com.resources.mvp.BasePresenter;
import com.resources.utils.MessageUtils;
import com.totalplay.modulepayandroid.R;
import com.totalplay.modulepayandroid.Utils.DatesUtils;
import com.totalplay.modulepayandroid.background.WSManagerNewEnvironment;
import com.totalplay.modulepayandroid.background.models.Response.History.HistoryItem;
import com.totalplay.modulepayandroid.background.models.Response.History.HistoryResponse;
import com.totalplay.modulepayandroid.background.webServices;
import com.totalplay.modulepayandroid.modules.main.models.Order;

import java.util.ArrayList;

public class HistoryPresenter extends BasePresenter implements WSCallback {

    private HistoryCallback historyCallback;
    private AppCompatActivity mContext;

    public HistoryPresenter(AppCompatActivity appCompatActivity, HistoryCallback historyCallback) {
        super(appCompatActivity);
        this.historyCallback = historyCallback;
        this.mContext = appCompatActivity;
    }

    public void getHistory(String id) {
        MessageUtils.progress(mContext, "Cargando");
        WSManagerNewEnvironment.init().settings(mContext, this)
                .requestWs(HistoryResponse.class, WSManagerNewEnvironment.WS.HISTORY_ORDERS,
                        webServices.services().getHistoryOrders(id, 0));
    }

    @Override
    public void onRequestWS(String requestUrl) {

    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        MessageUtils.stopProgress();
        if (requestUrl.equals(WSManagerNewEnvironment.WS.HISTORY_ORDERS)) {
            ArrayList<Order> orders = new ArrayList<>();
            HistoryResponse historyResponse = (HistoryResponse) baseResponse;
            if (historyResponse.getHistory() != null) {
                for (HistoryItem history : historyResponse.getHistory()) {
                    Order order = new Order();
                    order.setBussiness(history.getReference());
                    order.setPay(getPayCustomStr(history.getAmount()));
                    order.setDate(getDataCustom(history.getDate()));
                    orders.add(order);
                }
                historyCallback.onSuccessHistory(orders);
            }
        } else {
            historyCallback.onErrorHistory(R.string.error_response_query_history);
        }

    }

    private String getPayCustomStr(String orderAmount) {
        return "-$" + orderAmount;
    }

    private String getDataCustom(String paySuccessTime) {
        return DatesUtils.getData(paySuccessTime);
    }

    @Override
    public void onErrorLoadResponse(String requestUrl, String messageError) {
        MessageUtils.stopProgress();
        historyCallback.onErrorHistory(R.string.error_response_query_history);

    }

    @Override
    public void onErrorConnection() {
        MessageUtils.stopProgress();
        historyCallback.onErrorConnection();

    }

    public interface HistoryCallback {
        void onSuccessHistory(ArrayList<Order> response);

        void onErrorConnection();

        void onErrorHistory(int resMsg);

        void onLoadResponse();
    }
}
