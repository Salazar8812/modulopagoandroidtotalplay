package com.totalplay.modulepayandroid.modules.divideExpense.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.resources.mvp.BasePresenter;
import com.totalplay.modulepayandroid.MainAppCompatActivity;
import com.totalplay.modulepayandroid.R;
import com.totalplay.modulepayandroid.common.model.pojo.User;
import com.totalplay.modulepayandroid.data.PreferenceHelper;
import com.totalplay.modulepayandroid.modules.divideExpense.models.Contact;
import com.totalplay.modulepayandroid.modules.divideExpense.models.request.PayersItem;
import com.totalplay.modulepayandroid.modules.divideExpense.models.response.PayersRequestResponse;
import com.totalplay.modulepayandroid.modules.divideExpense.presenters.PayersRequestPresenter;
import com.totalplay.modulepayandroid.modules.divideExpense.view.adapters.ContactsMoneyAdapter;
import com.totalplay.modulepayandroid.modules.main.view.activities.MyAccountNewActivity;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PersonDivideActivity extends MainAppCompatActivity implements PayersRequestPresenter.View {

    public static final String USERS = "users";
    public static final String MONTO = "monto";
    public static final String CB_YO = "cbYo";
    public static final String REFERENCE = "reference";
    public static final String ORDER = "order";
    @BindView(R.id.rv_person_divide)
    RecyclerView rvPersonDivide;
    @BindView(R.id.tv_amount_person_divide)
    TextView tvAmountPersonDivide;

    @BindView(R.id.mDividirButton)
    Button btnmDividirButton;
    @BindView(R.id.mExitButton)
    Button btnmExitButton;
    @BindView(R.id.progresss)
    ConstraintLayout progresss;

    private ContactsMoneyAdapter contactsMoneyAdapter;

    private PayersRequestPresenter payersRequestPresenter;

    private float monto = 0f;
    private String mReferencia;
    private String mOrder;
    private boolean cbYo = false;
    private ArrayList<Contact> contacts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.activity_person_divide);
        ButterKnife.bind(this);
        getMonto();
        initProgress();
        getIntents();
        getCheckBox();
        managerContacts();
    }

    private void initProgress() {
        progress(false);
        enabledElements(true);
    }

    private void getIntents() {
        mOrder = getIntent().getStringExtra(ORDER);
        mReferencia = getIntent().getStringExtra(REFERENCE);
    }

    private void getCheckBox() {
        cbYo = getIntent().getExtras().getBoolean(CB_YO);
    }

    @Override
    protected BasePresenter getPresenter() {
        payersRequestPresenter = new PayersRequestPresenter(this, this);
        return payersRequestPresenter;
    }

    @Override
    protected void onResume() {
        super.onResume();
        onSetActivity(this);
        onSetTitle("DIVIDIR CUENTA");
    }


    private void getMonto() {
        monto = getIntent().getExtras().getFloat(MONTO);
        DecimalFormat df = new DecimalFormat("#.00");
        tvAmountPersonDivide.setText("$" + df.format(monto));
    }

    private Contact getContactYo(int size) {
        Contact contact = new Contact();
        contact.setId(PreferenceHelper.getString(User.UID));
        contact.setFavorite(false);
        contact.setSelectPay(false);
        contact.setAmount(Double.parseDouble(cbYo ? getFormatDec(monto / (size + 1)) : getFormatDec(monto / size)));
        contact.setName("(Yo)");
        return contact;
    }

    private String getPartByComensal(int comensales, float monto) {
        return getFormatDec(monto / comensales);
    }

    private String getFormatDec(float monto) {
        DecimalFormat df = new DecimalFormat("#.00");
        return df.format(monto);
    }

    private int getNumberComensales(ArrayList<Contact> contacts) {
        return cbYo ? contacts.size() + 1 : contacts.size();
    }


    private void managerContacts() {
        ArrayList<Contact> contactsAdapter = new ArrayList<>();
        ArrayList<Contact> contactsInit;
        ArrayList<Contact> contactsService;

        contactsInit = Objects.requireNonNull(getIntent().getParcelableArrayListExtra(USERS));

        if (cbYo) {
            contactsAdapter.add(getContactYo(contactsInit.size()));
        }
        contactsAdapter.addAll(contactsInit);
        initApdater(contactsAdapter);
        contactsService = contactsPart(contactsInit);
        contacts = contactsService;
    }

    private ArrayList<Contact> contactsPart(ArrayList<Contact> contacts) {
        ArrayList<Contact> contactsPay = new ArrayList<>();
        String part = getPartByComensal(getNumberComensales(contacts), monto);
        for (Contact contact : contacts) {
            contact.setAmount(Double.parseDouble(part));
            contactsPay.add(contact);
        }
        return contactsPay;
    }

    private void initApdater(ArrayList<Contact> contacts) {
        rvPersonDivide.setLayoutManager(new LinearLayoutManager(this));
        contactsMoneyAdapter = new ContactsMoneyAdapter(contacts);
        rvPersonDivide.setAdapter(contactsMoneyAdapter);
    }

    @OnClick({R.id.mDividirButton, R.id.mExitButton})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mDividirButton:
                enabledElements(false);
                progress(true);
                payersRequestPresenter.sendRequestToPayeers(
                        getContactsPay(this.contacts),
                        PreferenceHelper.getString(User.UID),
                        mReferencia,
                        monto,
                        mOrder
                );
                break;
            case R.id.mExitButton:
                Intent newIntent = new Intent(this, MyAccountNewActivity.class);
                newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(newIntent);
                break;
        }
    }

    private void enabledElements(boolean b) {
        btnmDividirButton.setEnabled(b);
        btnmExitButton.setEnabled(b);
    }

    private void progress(boolean b) {
        progresss.setVisibility(b ? View.VISIBLE : View.GONE);
    }

    private ArrayList<PayersItem> getContactsPay(ArrayList<Contact> contacts) {
        ArrayList<PayersItem> payers = new ArrayList<>();
        for (Contact contact : contacts) {
            PayersItem payersItem = new PayersItem();
            payersItem.setPayer(contact.getId());
            payersItem.setAmount(contact.getAmount());
            payers.add(payersItem);
        }
        return payers;
    }

    @Override
    public void onSuccessRequestSend(PayersRequestResponse payersRequestResponse) {
        progress(false);
        enabledElements(true);
        Toast.makeText(this, payersRequestResponse.getMessage(), Toast.LENGTH_SHORT).show();
        Intent i = new Intent(this , SuccessfulPaymentRequestActivity.class);
        i.putExtra(SuccessfulPaymentRequestActivity.NO_REFERENCE, mReferencia);
        startActivity(new Intent(this, SuccessfulPaymentRequestActivity.class));
    }

    @Override
    public void onErrorRequestSend(String msg) {
        progress(false);
        enabledElements(true);
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}