package com.totalplay.modulepayandroid.modules.login.view;

public interface AuthRegisterView {
    void onSucessRegister(String uid);
    void onError(int msg);
}
