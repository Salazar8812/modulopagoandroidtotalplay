package com.totalplay.modulepayandroid.modules.divideExpense.view.fragments;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.totalplay.modulepayandroid.R;
import com.totalplay.modulepayandroid.Utils.Constantes;
import com.totalplay.modulepayandroid.common.model.pojo.User;
import com.totalplay.modulepayandroid.data.PreferenceHelper;
import com.totalplay.modulepayandroid.modules.divideExpense.models.Contact;
import com.totalplay.modulepayandroid.modules.divideExpense.presenters.ContactsPresenter;
import com.totalplay.modulepayandroid.modules.divideExpense.view.DataUsers;
import com.totalplay.modulepayandroid.modules.divideExpense.view.adapters.ContactsAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentAll extends Fragment implements ContactsAdapter.onItemClickListener, ContactsPresenter.View {
    private static final String FRAGMENT_ALL = "all";
    @BindView(R.id.mRecyclerAll)
    RecyclerView mRecyclerAll;

    @BindView(R.id.et_fragment_all)
    EditText etFragmentAll;

    ContactsAdapter mContactsAdapter;
    ArrayList<Contact> contacts = new ArrayList<>();
    private DataUsers callback;

    private static final int LIST_GENERAL = 207;
    private static final int LIST_FILTER = 217;
    private int LIST = 227;

    private ContactsPresenter contactsPresenter;

    public static FragmentAll newInstance() {
        return new FragmentAll();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_all, container, false);
        ButterKnife.bind(this, v);
        createAdapter();
        intiPresenter();
        return v;
    }

    private void intiPresenter() {
        contactsPresenter = new ContactsPresenter(getActivity(), this);
        contactsPresenter.getContacts(PreferenceHelper.getString(User.UID));
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        searchviewManager();
    }

    private void searchviewManager() {
        etFragmentAll.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (etFragmentAll.getText().toString().isEmpty()) {
                    LIST = LIST_GENERAL;
                    mContactsAdapter.update(contacts);
                } else {
                    LIST = LIST_FILTER;
                    mContactsAdapter.getFilter().filter(etFragmentAll.getText().toString());
                }
            }
        });

    }


    void createAdapter() {
        mRecyclerAll.setLayoutManager(new LinearLayoutManager(getActivity()));
        mContactsAdapter = new ContactsAdapter(contacts, this);
        mRecyclerAll.setAdapter(mContactsAdapter);
    }

    private ArrayList<Contact> getListContacts() {
        ArrayList<Contact> contacts = new ArrayList<>();
        contacts.add(new Contact("luis", "13", false, false));
        contacts.add(new Contact("Gerardo", "14", false, false));
        contacts.add(new Contact("Jose", "15", false, false));
        contacts.add(new Contact("Maria", "16", false, false));
        contacts.add(new Contact("Jose miguel", "17", false, false));
        contacts.add(new Contact("Maria O", "18", false, false));
        return contacts;
    }

    @Override
    public void onItemApplyClick(View v, boolean b) {

        if (LIST == LIST_FILTER) {
            ArrayList<Contact> contactsFilter = mContactsAdapter.getContactsFilter();
            int item = (int) v.getTag();
            Contact contact = contactsFilter.get(item);
            contact.setSelectPay(b);
            contactsFilter.set(item, contact);
            mContactsAdapter.update(contactsFilter);

        } else {
            int item = (int) v.getTag();
            Contact contact = contacts.get(item);
            contact.setSelectPay(b);
            contacts.set(item, contact);
            mContactsAdapter.update(contacts);
        }

        passData(contacts);
    }

    @Override
    public void onItemFavoriteClick(View v, boolean b) {
        int item = (int) v.getTag();
        Contact contact = contacts.get(item);
        contact.setFavorite(b);
        contacts.set(item, contact);
        mContactsAdapter.update(contacts);
        passData(contacts);
    }

    @Override
    public void onGetContacts(ArrayList<Contact> contacts) {
        this.contacts.addAll(contacts);
        mContactsAdapter.update(this.contacts);
        LIST = LIST_GENERAL;
    }

    @Override
    public void onError(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callback = (DataUsers) context;
    }

    public void passData(ArrayList<Contact> contacts) {
        callback.onDataUsers(contacts, FRAGMENT_ALL);
    }

}
