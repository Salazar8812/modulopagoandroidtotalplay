package com.totalplay.modulepayandroid.firebase.login;

public interface LoginView {
    void onBiometricEnabled();
    void onBiometricDisabled(String msg);

    void onAuthenticationError(String msg);
    void onAuthenticationSucceeded();
    void onAuthenticationFailed(String msg);

    void openMainActivity(String idHuawei);
    void openUILogin();

    void onErrorFireStore();

    void onErrorPass(String error);
}
