package com.totalplay.modulepayandroid.firebase.login;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.totalplay.modulepayandroid.common.model.BaseEventCallback;
import com.totalplay.modulepayandroid.common.model.GetUserCallback;
import com.totalplay.modulepayandroid.common.model.dataAccess.FirebaseFireStoreAPI;
import com.totalplay.modulepayandroid.common.model.pojo.User;
import com.totalplay.modulepayandroid.data.PreferenceHelper;
import com.totalplay.modulepayandroid.modules.divideExpense.models.ContactFS;
import com.totalplay.modulepayandroid.modules.divideExpense.models.contactsFSCallback;
import com.totalplay.modulepayandroid.modules.notifications.model.NotificationNetwork;
import com.totalplay.modulepayandroid.modules.notifications.model.RequestFSCallback;
import com.totalplay.modulepayandroid.modules.notifications.presenters.NumberNotificationsCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FireStore {

    private FirebaseFireStoreAPI mFirestoreAPI;

    public FireStore() {
        mFirestoreAPI = FirebaseFireStoreAPI.getInstance();
    }

    public void getUser(String uid, GetUserCallback callback) {
        mFirestoreAPI.getUsersReferences().document(uid).get()
                .addOnSuccessListener(documentSnapshot -> {
                    callback.onSuccess(documentSnapshot.toObject(User.class));
                })
                .addOnFailureListener(e ->
                        callback.onError(e.getMessage()));
    }

    public void getContactos(String uid, contactsFSCallback callback) {
        ArrayList<ContactFS> listContactsFS = new ArrayList<>();
        mFirestoreAPI.getContactosReferences(uid).addSnapshotListener((snapshots, error) -> {
            if (snapshots != null) {
                for (DocumentChange doc : snapshots.getDocumentChanges()) {
                    ContactFS contactFS = doc.getDocument().toObject(ContactFS.class);
                    listContactsFS.add(contactFS);
                }
                callback.onSuccessgetContacts(listContactsFS);
            } else {
                callback.onError();
            }
        });
    }

    public void getRequestsReceived(String uid, RequestFSCallback callback) {
        ArrayList<NotificationNetwork> listNotificationsnetwork = new ArrayList<>();
        mFirestoreAPI.getRequestReceivedByUid(uid).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.getResult() != null) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        NotificationNetwork notificationNetwork = document.toObject(NotificationNetwork.class);
                        notificationNetwork.setId(document.getId());
                        listNotificationsnetwork.add(notificationNetwork);
                    }
                    callback.onSuccessgetRequest(listNotificationsnetwork);
                } else {
                    callback.onError();
                }
            }
        });
/*
        mFirestoreAPI.getRequestReceivedByUid(uid).addSnapshotListener((snapshots, error) -> {
            if(snapshots != null){
                for(DocumentChange doc : snapshots.getDocumentChanges()){
                    NotificationNetwork notificationNetwork = doc.getDocument().toObject(NotificationNetwork.class);
                    notificationNetwork.setId(doc.getDocument().getId());
                    listNotificationsnetwork.add(notificationNetwork);
                }
                callback.onSuccessgetRequest(listNotificationsnetwork);
            }else{
                callback.onError();
            }
        });
*/
    }

    public void getRequestsSend(String uid, RequestFSCallback callback) {
        ArrayList<NotificationNetwork> listNotificationsnetwork = new ArrayList<>();
        mFirestoreAPI.getRequestSendByUid(uid).addSnapshotListener((snapshots, error) -> {
            if (snapshots != null) {
                for (DocumentChange doc : snapshots.getDocumentChanges()) {
                    NotificationNetwork notificationNetwork = doc.getDocument().toObject(NotificationNetwork.class);
                    listNotificationsnetwork.add(notificationNetwork);
                }
                callback.onSuccessgetRequest(listNotificationsnetwork);
            } else {
                callback.onError();
            }
        });
    }

/*
    public void getFlagConfig(FlagCallback callback){
        mFirestoreAPI.getConfigReference().addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException error) {
                if(error != null){
                    if(error.getCode() == FirebaseFirestoreException.Code.PERMISSION_DENIED){
                        callback.onErrorGetFlag("permiso denegado!");
                    }else{
                        callback.onErrorGetFlag("Error en el servidor");
                    }
                }else{
                    if (snapshots != null) {
                        callback.onGetFlag((Boolean) snapshots.getDocumentChanges().get(0).getDocument().getData().get(IRA_HUAWEI));
                    }else{
                        callback.onErrorGetFlag("snapshot invalid");
                    }
                }
            }
        });
    }
*/

    public void updateToken(final BaseEventCallback callback) {
        Map<String, Object> values = new HashMap<>();
        values.put(User.TKDISPOSITIVO, PreferenceHelper.getString(User.TKDISPOSITIVO));

        mFirestoreAPI.getUsersReferencesByUid(PreferenceHelper.getString(User.UID)).update(values).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                callback.onSuccess();
            } else {
                callback.onError();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                e.getMessage();
                Log.d("TAGEX", "exception : " + e.getMessage());
                callback.onError();
            }
        });
    }

    public void updateViewNotification(String docId, final BaseEventCallback callback) {
        Map<String, Object> values = new HashMap<>();
        values.put(User.NOTIFY_VIEW, true);
        mFirestoreAPI.getRequestByUid(PreferenceHelper.getString(User.UID), docId).update(values).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                callback.onSuccess();
            } else {
                callback.onError();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                e.getMessage();
                Log.d("TAGEX", "exception : " + e.getMessage());
                callback.onError();
            }
        });
    }

    public void getNumberNotify(String uid, NumberNotificationsCallback numberNotificationsCallback) {
        mFirestoreAPI.getRequestReceivedByUid(uid)
                .whereEqualTo("vista", false)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.getResult() != null) {
                        numberNotificationsCallback.onGetNumber(task.getResult().getDocuments().size());
                    } else {
                        numberNotificationsCallback.onError("error al obtener numero de notificaciones");
                    }
                }).addOnFailureListener(e -> numberNotificationsCallback.onError(e.getMessage()));
    }
}
