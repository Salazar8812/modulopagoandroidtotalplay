package com.totalplay.modulepayandroid.firebase.Authentication.model.dataAccess;

public interface TokenCallback {
    void onSucessGetToken(String token);
    void onError(int resMsg);
}
