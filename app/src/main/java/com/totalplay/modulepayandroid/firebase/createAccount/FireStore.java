package com.totalplay.modulepayandroid.firebase.createAccount;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.totalplay.modulepayandroid.common.model.BaseEventCallback;
import com.totalplay.modulepayandroid.common.model.dataAccess.FirebaseFireStoreAPI;
import com.totalplay.modulepayandroid.common.model.pojo.User;

import java.util.HashMap;
import java.util.Map;

public class FireStore {
    private FirebaseFireStoreAPI mFirestoreAPI;

    public FireStore() {
        mFirestoreAPI = FirebaseFireStoreAPI.getInstance();
    }


    public void addUser(User user, final BaseEventCallback callback) {
        Map<String, Object> values = new HashMap<>();
/*
        values.put(User.NOMBRE, user.getNombre());
        values.put(User.APELLIDO1, user.getApellido1());
        values.put(User.APELLIDO2, user.getApellido2());
        values.put(User.EMAIL, user.getEmail());
        values.put(User.IDHUAWEI, user.getIdHuawei());
*/
        values.put(User.TKDISPOSITIVO, user.getTokenDispositivo());


        mFirestoreAPI.getUsersReferencesByUid(user.getUid()).update(values)/*
        mFirestoreAPI.getUsersReferences().add(values)*/.addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                callback.onSuccess();
            } else {
                callback.onError();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                e.getMessage();
                Log.d("TAGEX", "exception : " + e.getMessage());
                callback.onError();
            }
        });
    }
}
