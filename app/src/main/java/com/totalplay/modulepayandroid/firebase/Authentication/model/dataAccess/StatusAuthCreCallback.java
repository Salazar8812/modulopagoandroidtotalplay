package com.totalplay.modulepayandroid.firebase.Authentication.model.dataAccess;

import com.google.firebase.auth.FirebaseUser;

public interface StatusAuthCreCallback {
    void onGetUser(FirebaseUser user);
    void onError(String resMsg);

}
