package com.totalplay.modulepayandroid.firebase.createAccount;

public interface CreateAcountView {
    void onGetUser();
    void onError();
}
