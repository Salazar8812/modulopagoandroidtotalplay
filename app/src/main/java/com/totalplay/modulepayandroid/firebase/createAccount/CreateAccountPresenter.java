package com.totalplay.modulepayandroid.firebase.createAccount;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.iid.FirebaseInstanceId;
import com.resources.mvp.BasePresenter;
import com.totalplay.modulepayandroid.firebase.Authentication.model.dataAccess.Authentication;
import com.totalplay.modulepayandroid.common.model.BaseEventCallback;
import com.totalplay.modulepayandroid.common.model.pojo.User;

public class CreateAccountPresenter extends BasePresenter {

    private Authentication authentication;
    private FireStore mFireStore;
    private String tkDispositivo = "";

    public CreateAccountPresenter(AppCompatActivity appCompatActivity) {
        super(appCompatActivity);
        authentication = new Authentication();
        mFireStore = new FireStore();
        getDeviceIdToken();
    }

    private void getDeviceIdToken() {
        FirebaseInstanceId.getInstance()
                .getInstanceId()
                .addOnSuccessListener(mAppCompatActivity, instanceIdResult ->
                        setToken(instanceIdResult.getToken()))
                .addOnFailureListener(mAppCompatActivity, e ->
                        setToken(null));
    }

    private void setToken(String token) {
        this.tkDispositivo=token;
    }

    private String getToken(){
        return this.tkDispositivo;
    }

    //checar database user existe
    //si no existe lo registras

    public void registerUser(CreateAcountView createAcountView, User user, String pass) {
        User myUser = new User();
        myUser.setNombre(user.getNombre());
        myUser.setApellido1(user.getApellido1());
        myUser.setApellido2(user.getApellido2());
        myUser.setEmail(user.getEmail());
        myUser.setIdHuawei(user.getIdHuawei());
        myUser.setTokenDispositivo(getToken());

        mFireStore.addUser(myUser, new BaseEventCallback() {
            @Override
            public void onSuccess() {
                createAcountView.onGetUser();
            }

            @Override
            public void onError() {
                createAcountView.onError();
            }
        });

/*
        authentication.register(new StatusAuthRegisterCallback() {
            @Override
            public void onGetUser(FirebaseUser firebaseUser) {
                User myUser = new User();
                myUser.setUid(firebaseUser.getUid());
                myUser.setNombre(user.getNombre());
                myUser.setApellido1(user.getApellido1());
                myUser.setApellido2(user.getApellido2());
                myUser.setEmail(user.getEmail());
                myUser.setIdHuawei(getIdHUaweiByUser(user.getNombre()));
                myUser.setTokenDispositivo(getToken());
                mFireStore.addUser(myUser, new BaseEventCallback() {
                    @Override
                    public void onSuccess() {
                        createAcountView.onGetUser();
                    }

                    @Override
                    public void onError() {
                        createAcountView.onError();
                    }
                });
            }

            @Override
            public void onError() {
                createAcountView.onError();
            }
        }, user.getEmail(), pass);
*/
    }

    private String getIdHUaweiByUser(String mUser) {
        switch (mUser) {
            case "Aurelio":
                return "8615000000218";
            case "aurelio":
                return "8615000000218";
            case "Eduardo":
                return "8615000000053";
            case "eduardo":
                return "8615000000053";

            /***/
            case "Arte helada":
                return "100002301";

            case "arte helada":
                return "100002301";

            case "Arte Helada":
                return "100002301";

            case "arte Helada":
                return "100002301";

            case "Doña blanca":
                return "100002303";
            case "doña blanca":
                return "100002303";

            case "Doña Blanca":
                return "100002303";

            case "doña Blanca":
                return "100002303";
        }
        return "";
    }
}
