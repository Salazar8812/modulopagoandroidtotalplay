package com.totalplay.modulepayandroid.firebase;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.biometric.BiometricManager;
import androidx.biometric.BiometricPrompt;
import androidx.core.content.ContextCompat;

import com.resources.mvp.BasePresenter;
import com.totalplay.modulepayandroid.Utils.BiometricView;

import java.util.concurrent.Executor;


public class BiometricPresenter extends BasePresenter {

    private AppCompatActivity mAppcompactActivity;
    private BiometricView biometricView;

    public BiometricPresenter(AppCompatActivity appCompatActivity, BiometricView biometricView) {
        super(appCompatActivity);
        this.mAppcompactActivity = appCompatActivity;
        this.biometricView = biometricView;
    }



    public void validationBiometricExist() {
        BiometricManager biometricManager = BiometricManager.from(mAppcompactActivity);
        switch (biometricManager.canAuthenticate()) {
            case BiometricManager.BIOMETRIC_SUCCESS:
                biometricView.onBiometricEnabled();
                break;
            case BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE:
                biometricView.onBiometricDisabled("No hay características biométricas disponibles en este dispositivo.");
                break;
            case BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE:
                biometricView.onBiometricDisabled("Las características biométricas no están disponibles actualmente");
                break;
            case BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED:
                biometricView.onBiometricDisabled("El usuario no ha asociado ninguna credencial biométrica con su cuenta.");
                break;
        }
    }

    public BiometricPrompt getBiometricPrompt() {
        Executor executor = ContextCompat.getMainExecutor(mAppcompactActivity);
        return new BiometricPrompt(mAppcompactActivity,
                executor, new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode, @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                biometricView.onAuthenticationError("Error al realizar la autentificación");
            }

            @Override
            public void onAuthenticationSucceeded(@NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                biometricView.onAuthenticationSucceeded();
            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
                biometricView.onAuthenticationFailed("el dedo se movió demasiado rápido !");

            }
        });
    }

    public BiometricPrompt.PromptInfo getPrompInfo() {
        return new BiometricPrompt.PromptInfo.Builder()
                .setTitle("Autentificación biometrica")
                .setSubtitle("")
                .setNegativeButtonText("Cancelar")
                .build();
    }

}
