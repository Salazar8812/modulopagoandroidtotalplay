package com.totalplay.modulepayandroid.firebase.Authentication.model.dataAccess;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.totalplay.modulepayandroid.R;
import com.totalplay.modulepayandroid.common.model.dataAccess.FirebaseAuthenticationAPI;
import com.totalplay.modulepayandroid.common.model.pojo.User;
import com.totalplay.modulepayandroid.data.PreferenceHelper;

public class Authentication {
    private FirebaseAuthenticationAPI mAuthenticationAPI;

    private static final String EMAIL_FORMATED_ERROR = "The email address is badly formatted.";
    private static final String EMAIL_NO_EXIST_ERROR = "There is no user record corresponding to this identifier. The user may have been deleted.";
    private static final String PASSWORD_ERROR = "The password is invalid or the user does not have a password.";

    public Authentication() {
        mAuthenticationAPI = FirebaseAuthenticationAPI.getInstance();
        mAuthenticationAPI.getFirebaseAuth().addAuthStateListener(getListener());
    }



    private FirebaseAuth.AuthStateListener getListener() {
        return new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = mAuthenticationAPI.getCurrentUser();
                if (user != null) {
                    PreferenceHelper.saveBoolean(User.LOGGED, true);
                } else {
                    PreferenceHelper.saveBoolean(User.LOGGED, false);
                }
            }
        };
    }

    public void getAuthWithcredenciales(String email, String pass, StatusAuthCreCallback callback) {
        mAuthenticationAPI.getFirebaseAuth().signInWithEmailAndPassword(email, pass).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                FirebaseUser user = mAuthenticationAPI.getFirebaseAuth().getCurrentUser();
                if (user != null) {
                    callback.onGetUser(user);
                } else {
                    callback.onError("error");
                }
            }
        }).addOnFailureListener(e -> {
            if(e.getMessage() != null){
                switch (e.getMessage()){
                    case EMAIL_FORMATED_ERROR:
                        callback.onError("Email no válido");
                        break;
                    case EMAIL_NO_EXIST_ERROR:
                        callback.onError("Usuario no registrado");
                        break;
                    case PASSWORD_ERROR:
                        callback.onError("Usuario o contraseña no válido");
                }
            }else{
                callback.onError("Error de authenticacion !");
            }
        });
    }

    public void register(String email, String password, StatusAuthRegisterCallback callback) {
        mAuthenticationAPI.getFirebaseAuth().createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = mAuthenticationAPI.getCurrentUser();
                            callback.onGetUser(user);
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                callback.onError();
            }
        });
    }

    public void getToken(TokenCallback tokenCallback) {
        if(mAuthenticationAPI.getFirebaseAuth() != null && mAuthenticationAPI.getFirebaseAuth().getCurrentUser() != null){
            mAuthenticationAPI.getFirebaseAuth()
                    .getCurrentUser()
                    .getIdToken(true).addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            if(task.getResult() != null)
                                tokenCallback.onSucessGetToken(task.getResult().getToken());
                            else
                                tokenCallback.onError(R.string.error_token_result_null);
                        }
                    }).addOnFailureListener(e -> tokenCallback.onError(R.string.error_token_auth));
        }
    }


}
