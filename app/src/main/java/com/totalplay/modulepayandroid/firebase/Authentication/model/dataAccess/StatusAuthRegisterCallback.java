package com.totalplay.modulepayandroid.firebase.Authentication.model.dataAccess;

import com.google.firebase.auth.FirebaseUser;

public interface StatusAuthRegisterCallback {
    void onGetUser(FirebaseUser user);
    void onError();
}
