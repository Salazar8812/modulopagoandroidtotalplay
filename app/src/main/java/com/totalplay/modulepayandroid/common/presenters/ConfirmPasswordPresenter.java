package com.totalplay.modulepayandroid.common.presenters;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseUser;
import com.resources.mvp.BasePresenter;
import com.totalplay.modulepayandroid.firebase.Authentication.model.dataAccess.Authentication;
import com.totalplay.modulepayandroid.firebase.Authentication.model.dataAccess.StatusAuthCreCallback;
import com.totalplay.modulepayandroid.modules.login.view.AuthView;

public class ConfirmPasswordPresenter {

    private Authentication mAuthentication;
    private AuthView authView;

    public ConfirmPasswordPresenter() {
        this.mAuthentication = new Authentication();
    }

    public void loginWithUserAndPass(String user, String pass, AuthView authView) {
        mAuthentication.getAuthWithcredenciales(user, pass, new StatusAuthCreCallback() {
            @Override
            public void onGetUser(FirebaseUser user) {
                authView.onSucess(user.getUid());
            }

            @Override
            public void onError(String resMsg) {
                authView.onError(resMsg);
            }
        });
    }





}
