package com.totalplay.modulepayandroid.common.model;

public interface BaseEventCallback {
    void onSuccess();
    void onError();
}
