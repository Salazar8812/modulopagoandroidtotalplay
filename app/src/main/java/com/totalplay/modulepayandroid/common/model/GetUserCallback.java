package com.totalplay.modulepayandroid.common.model;

import com.totalplay.modulepayandroid.common.model.pojo.User;

public interface GetUserCallback {
    void onSuccess(User user);
    void onError(String resMsg);
}

