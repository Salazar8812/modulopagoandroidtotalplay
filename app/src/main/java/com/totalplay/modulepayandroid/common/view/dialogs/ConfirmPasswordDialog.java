package com.totalplay.modulepayandroid.common.view.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.resources.utils.MessageUtils;
import com.resources.utils.validators.EditTextValidator;
import com.resources.utils.validators.FormValidator;
import com.resources.utils.validators.Regex;
import com.totalplay.modulepayandroid.common.presenters.ConfirmPasswordPresenter;
import com.totalplay.modulepayandroid.modules.login.view.AuthView;
import com.totalplay.modulepayandroid.modules.pay.view.activities.PayActivity;
import com.totalplay.modulepayandroid.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ConfirmPasswordDialog extends DialogFragment {

    @BindView(R.id.et_dialog_confirm_password)
    EditText etDialogConfirmPassword;
    @BindView(R.id.et_dialog_confirm_user)
    EditText etDialogConfirmUser;
    @BindView(R.id.iv_dialog_confirm_visible_pass)
    ImageView ivDialogConfirmVisiblePass;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    private boolean isVisibility = false;
    private PayActivity.payCredentials mPayCredentials;
    private ConfirmPasswordPresenter confirmPasswordPresenter;
    FormValidator mValidator;

    public static ConfirmPasswordDialog newInstance() {
        ConfirmPasswordDialog confirmPasswordDialog = new ConfirmPasswordDialog();
        Bundle b = new Bundle();
        confirmPasswordDialog.setArguments(b);
        return confirmPasswordDialog;
    }

    public void getAuthCredentials(PayActivity.payCredentials paycredentials) {
        mPayCredentials = paycredentials;
    }

    void addValidators() {
        mValidator.addValidator(new EditTextValidator(etDialogConfirmPassword, Regex.NOT_EMPTY, R.string.errorfields));
        mValidator.addValidator(new EditTextValidator(etDialogConfirmUser, Regex.NOT_EMPTY, R.string.errorfields));
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View v = inflater.inflate(R.layout.dialog_confirm_password, null);
        builder.setView(v);
        ButterKnife.bind(this, v);
        confirmPasswordPresenter = new ConfirmPasswordPresenter();
        mValidator = new FormValidator(getContext());
        addValidators();

        return builder.create();
    }


    @OnClick({R.id.iv_dialog_confirm_visible_pass, R.id.btn_dialog_confirm_transaction})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_dialog_confirm_visible_pass:
                if (isVisibility) {
                    ivDialogConfirmVisiblePass.setImageDrawable(getResources().getDrawable(R.drawable.ic_icon_totalpay_visibility_off));
                    etDialogConfirmPassword.setTransformationMethod(new PasswordTransformationMethod());
                } else {
                    ivDialogConfirmVisiblePass.setImageDrawable(getResources().getDrawable(R.drawable.ic_icon_totalplay_visivility_password));
                    etDialogConfirmPassword.setTransformationMethod(null);
                }
                etDialogConfirmPassword.setSelection(etDialogConfirmPassword.getText().length());
                isVisibility = !isVisibility;
                break;
            case R.id.btn_dialog_confirm_transaction:
                if (mValidator.isValid()) {
                    confirmPasswordPresenter.loginWithUserAndPass(getTxt(etDialogConfirmUser), getTxt(etDialogConfirmPassword), new AuthView() {
                        @Override
                        public void onSucess(String uid) {
                            mPayCredentials.onSuccess();
                        }

                        @Override
                        public void onError(String resMsg) {
                            mPayCredentials.onFailure(resMsg);
                        }
                    });

                } else {
                    mPayCredentials.onFailure("Debe llenar todos los campos obligatorios");
                }
                this.dismiss();
                break;
        }
    }

    private String getTxt(EditText etDialogConfirmPassword) {
        return etDialogConfirmPassword.getText().toString().trim();
    }

}
