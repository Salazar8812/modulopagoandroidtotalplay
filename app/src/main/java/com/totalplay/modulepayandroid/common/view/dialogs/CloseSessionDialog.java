package com.totalplay.modulepayandroid.common.view.dialogs;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.google.firebase.auth.FirebaseAuth;
import com.resources.utils.Prefs;
import com.totalplay.modulepayandroid.R;
import com.totalplay.modulepayandroid.common.model.pojo.User;
import com.totalplay.modulepayandroid.data.PreferenceHelper;
import com.totalplay.modulepayandroid.modules.login.view.activities.LoginActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CloseSessionDialog extends DialogFragment {

    @BindView(R.id.mCloseSessionButton)
    Button mCloseSessionButton;
    @BindView(R.id.mCancelCloseSessionButton)
    Button mCancelCloseSessionButton;

    public static CloseSessionDialog newInstance() {
        CloseSessionDialog v = new CloseSessionDialog();
        Bundle b = new Bundle();
        v.setArguments(b);
        return v;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View newFileView = inflater.inflate(R.layout.close_session_dialog, null);
        builder.setView(newFileView);
        ButterKnife.bind(this, newFileView);

        return builder.create();

    }


    @OnClick({R.id.mCloseSessionButton, R.id.mCancelCloseSessionButton})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mCloseSessionButton:
                //cerrar session en firebase.
                FirebaseAuth.getInstance().signOut();
                //eliminar preferencias datos de usuario.
                Intent newIntent = new Intent(getContext(), LoginActivity.class);
                newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK |
                        Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(newIntent);
                break;
            case R.id.mCancelCloseSessionButton:
                dismissAllowingStateLoss();
                break;
        }
    }
}
