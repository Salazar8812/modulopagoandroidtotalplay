package com.totalplay.modulepayandroid.common.model.dataAccess;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

public class FirebaseFireStoreAPI {

    private static String COLL__USUARIOS  = "usuarios";
    private static String COLL__CONTACTS  = "contactos";
    private static String COLL__REQUESTS  = "solicitudes";
    private static String COLL__REQUESTS_SENDS  = "solicitudesEnviadas";
    private static String COLL__REQUESTS_RECEIVED  = "solicitudesRecibidas";

    private FirebaseFirestore mFirestore;

    private static FirebaseFireStoreAPI INSTANCE =  null;

    private FirebaseFireStoreAPI(){
        mFirestore = FirebaseFirestore.getInstance();
    }

    /*patron de diseno singleton*/
    public static FirebaseFireStoreAPI getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new FirebaseFireStoreAPI();
        }
        return INSTANCE;
    }

    public FirebaseFirestore getmFirestore() {
        return mFirestore;
    }

    public CollectionReference getUsersReferences() {
        return mFirestore.collection(COLL__USUARIOS);
    }

    public CollectionReference getContactosReferences(String uid) {
        DocumentReference ref = getUsersReferences().document(uid);
        return ref.collection(COLL__CONTACTS);
    }

    public CollectionReference getRequestSendByUid(String uid) {
        DocumentReference ref = getUsersReferences().document(uid);
        return ref.collection(COLL__REQUESTS_SENDS);
    }

    public DocumentReference getRequestByUid(String uid, String docId) {
        DocumentReference ref = getUsersReferences().document(uid);
        return ref.collection(COLL__REQUESTS_RECEIVED).document(docId);
    }

    public CollectionReference getRequestReceivedByUid(String uid) {
        DocumentReference ref = getUsersReferences().document(uid);
        return ref.collection(COLL__REQUESTS_RECEIVED);
    }

    public DocumentReference getUsersReferencesByUid(String uid) {
        return mFirestore.collection(COLL__USUARIOS).document(uid);
    }

}
