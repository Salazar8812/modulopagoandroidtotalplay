package com.totalplay.modulepayandroid.common;

public class Constants {

    public static  final String IPTV = "iptv";
    public static  final String FLINK = "flink";
    public static  final String USERS = "users";

    public static  final String SERVICES_DOWN = "down";
    public static  final String DOWN = "down";
    public static  final String UP = "up";

}
