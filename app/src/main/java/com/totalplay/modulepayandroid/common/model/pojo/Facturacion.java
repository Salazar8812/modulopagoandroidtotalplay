package com.totalplay.modulepayandroid.common.model.pojo;

public class Facturacion {

    private String email;
    private String razonSocial;
    private String rfc;
    private String tipoPersona;
    private String usoCFDI;

    public static final String EMAIL = "emial";
    public static final String RAZON_SOCIAL = "razon_social";
    public static final String RFC = "rfc";
    public static final String TIPO_PERSONA = "tipo_persona";
    public static final String USO_CFDI = "uso_cfdi";


    public Facturacion() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public String getUsoCFDI() {
        return usoCFDI;
    }

    public void setUsoCFDI(String usoCFDI) {
        this.usoCFDI = usoCFDI;
    }
}
