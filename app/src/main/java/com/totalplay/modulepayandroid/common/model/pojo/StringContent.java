package com.totalplay.modulepayandroid.common.model.pojo;

public class StringContent {

    private String idPlayer;
    private String pay;
    private Boolean fixedAmount;
    private String orderNo;

    public StringContent() {
    }

    public String getIdPlayer() {
        return idPlayer;
    }

    public void setIdPlayer(String idPlayer) {
        this.idPlayer = idPlayer;
    }

    public String getPay() {
        return pay;
    }

    public void setPay(String pay) {
        this.pay = pay;
    }

    public Boolean getFixedAmount() {
        return fixedAmount;
    }

    public void setFixedAmount(Boolean fixedAmount) {
        this.fixedAmount = fixedAmount;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }
}
