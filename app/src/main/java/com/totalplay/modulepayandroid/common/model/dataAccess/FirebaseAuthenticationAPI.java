package com.totalplay.modulepayandroid.common.model.dataAccess;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.totalplay.modulepayandroid.common.model.pojo.User;

public class FirebaseAuthenticationAPI {

    private FirebaseAuth mFirebaseAuth;


    private static class SingletonHolder{

        private static final FirebaseAuthenticationAPI INSTANCE = new FirebaseAuthenticationAPI();


    }

    public static FirebaseAuthenticationAPI getInstance(){
        return SingletonHolder.INSTANCE;
    }

    private FirebaseAuthenticationAPI(){
        this.mFirebaseAuth = FirebaseAuth.getInstance();
    }

    public FirebaseAuth getFirebaseAuth() {
        return this.mFirebaseAuth;
    }

    public FirebaseUser getCurrentUser() {
        return mFirebaseAuth.getCurrentUser();
    }

}
