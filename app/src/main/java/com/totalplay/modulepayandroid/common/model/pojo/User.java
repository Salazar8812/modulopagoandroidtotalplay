package com.totalplay.modulepayandroid.common.model.pojo;

import com.google.firebase.database.Exclude;

public class User {

    public static final String APELLIDO1 = "apellido1";
    public static final String APELLIDO2 = "apellido2";
    public static final String EMAIL = "email";

    public static final String FACTURACION = "facturacion";

    public static final String IDHUAWEI = "idHuawei";
    public static final String IDMAMBU = "idMambu";
    public static final String NOMBRE = "nombre";
    public static final String CATEGORIA = "caategoria";

    public static final String TIPO = "tipo";
    public static final String TKDISPOSITIVO = "tokenDispositivo";
    public static final String TKA = "tokenA";
    public static final String LOGGED = "logged";
    public static final String NOTIFY_VIEW = "vista";



    public static final String UID = "uid";

    private String apellido1;
    private String apellido2;
    private String email;
    private String categoria;
    private Facturacion facturacion;

    private String idHuawei;
    private String idMambu;
    private String nombre;
    private String tipo;
    private String tokenDispositivo;

    @Exclude
    private String uid;

    public User() {
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getIdMambu() {
        return idMambu;
    }

    public void setIdMambu(String idMambu) {
        this.idMambu = idMambu;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Facturacion getFacturacion() {
        return facturacion;
    }

    public void setFacturacion(Facturacion facturacion) {
        this.facturacion = facturacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTokenDispositivo() {
        return tokenDispositivo;
    }

    public void setTokenDispositivo(String tokenDispositivo) {
        this.tokenDispositivo = tokenDispositivo;
    }

    public String getIdHuawei() {
        return idHuawei;
    }

    public void setIdHuawei(String idHuawei) {
        this.idHuawei = idHuawei;
    }

    @Exclude
    public String getUid() {
        return uid;
    }

    @Exclude
    public void setUid(String uid) {
        this.uid = uid;
    }
}

