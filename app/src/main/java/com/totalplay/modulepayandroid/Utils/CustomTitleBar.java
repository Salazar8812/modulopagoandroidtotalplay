package com.totalplay.modulepayandroid.Utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.TextView;

@SuppressLint("AppCompatCustomView")
public class CustomTitleBar extends TextView {

    public static TextView mTextView;

    public CustomTitleBar(Context context) {
        super(context);
        mTextView = this;
    }

    public CustomTitleBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        mTextView = this;
    }

    public CustomTitleBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mTextView = this;
    }

    protected void onDraw (Canvas canvas) {
        super.onDraw(canvas);
    }
}

