package com.totalplay.modulepayandroid.Utils;

public interface BiometricView {
    void onBiometricEnabled();
    void onBiometricDisabled(String msg);

    void onAuthenticationError(String msg);
    void onAuthenticationSucceeded();
    void onAuthenticationFailed(String msg);

}
