package com.totalplay.modulepayandroid.Utils;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.regex.Pattern;

public class OperationsUtils {



    //metodos
    //5,000.20 -> 5000                  //metodo regresa cantidad entera del pago
    //5,000.20 -> 0.20                  //metodo regresa cantidad centavos del pago

    //5,000 -> porcentaje de 5000       //metodo regresa cantidad porcentual de enteros 5000
    //5,000.20 -> porcentaje de 0.20    // metodo regresa cantidad porcentual de centavos 0.20


    //5000 -> 5,000.00                  //metodo regresa cantidad con comas y punto.
    //5000.00 -> 5,000.00               //metodo regresa cantidad con comas y punto.
    //5000.50 -> 5,000.50               //metodo regresa cantidad con comas y punto.


    public static String getCurrentBalance(String currentBalance) {
        /*
        currentBalance = "123456789.90"; //123,456,789.90
        currentBalance = "23456789.90"; //23,456,789.90
        currentBalance = "3456789.90"; //3,456,789.90
        currentBalance = "456789.90"; //456,789.90
        currentBalance = "0.0"; //456,789.00
*/
        currentBalance = getStrSinComas(currentBalance);
        String balanceInt = getBalancePart(currentBalance, true);
        String balanceFloat = getBalancePart(currentBalance, false);
        String strInv = getStrInv(balanceInt);
        String strComas = getStrComas(strInv);
        String strReal = getStrInv(strComas) + "." + balanceFloat;
        if (strReal.substring(0, 1).equals(",")) {
            strReal = strReal.substring(1);
        }
        return strReal;
    }

    public static String getStrSinComas(String currentBalance) {
        String[] parts = currentBalance.split(",");
        StringBuilder current = new StringBuilder();
        for (String part : parts) {
            current.append(part);
        }
        return String.valueOf(current);
    }

    private static String getBalancePart(String currentBalance, boolean integer) {
        if (currentBalance.equals("0")) {
            return "0";
        }

        String separador = Pattern.quote(".");
        String[] parts = currentBalance.split(separador);
        if (parts.length == 2) {
            if (integer) {
                return parts[0];
            } else {
                if(parts[1].length()==1){
                    return parts[1] + "0";
                }
                return parts[1].substring(0, 2);
            }
        } else {
            if (integer) {
                return parts[0];
            } else {
                return "00";
            }

        }
    }

    private static String getStrComas(String strInv) {
        int j = 0;
        String strWithComas = "";
        for (int i = 0; i < strInv.length(); i++) {
            strWithComas = strWithComas + strInv.charAt(i);
            j++;
            if (j == 3) {
                j = 0;
                strWithComas = strWithComas + ",";
            }
        }
        return strWithComas;
    }

    private static String getStrInv(String balanceInt) {
        StringBuilder strInv = new StringBuilder();
        for (int x = balanceInt.length() - 1; x >= 0; x--)
            strInv.append(balanceInt.charAt(x));

        return strInv.toString();
    }

    public static String getPropina(String amount, int propina) {
        amount = amount.substring(1).trim();
        amount = getStrSinComas(amount);
        float amountStr = Float.parseFloat(amount);
        String propinaSinComas = geStrTwoDecimals(String.valueOf(amountStr * (propina / 100.0f)));
        String propinaConComasyPunto = getCurrentBalance(propinaSinComas);
        return "$" + propinaConComasyPunto;
    }

    public static String geStrTwoDecimals(String number) {
/*        number = "7.712345";
        number = "7.71";
        number = "7.0";
        number = "7";
*/
        String separador = Pattern.quote(".");
        String[] parts = number.split(separador);

        if (parts.length > 1) {
            if (parts[1].length() == 1) {
                return parts[0] + "." + parts[1] + "0";
            }
            return parts[0] + "." + parts[1].substring(0, 2);
        }
        return parts[0] + ".00";
    }


}
