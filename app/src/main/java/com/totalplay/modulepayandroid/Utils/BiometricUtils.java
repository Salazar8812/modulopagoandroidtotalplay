package com.totalplay.modulepayandroid.Utils;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.biometric.BiometricManager;
import androidx.biometric.BiometricPrompt;
import androidx.core.content.ContextCompat;

import java.util.concurrent.Executor;

public class BiometricUtils {

    private AppCompatActivity mAppcompactActivity;

    public BiometricUtils(AppCompatActivity appCompatActivity) {
        this.mAppcompactActivity = appCompatActivity;
    }

    public void validationBiometricExist(BiometricCallback biometricCallback) {
        BiometricManager biometricManager = BiometricManager.from(mAppcompactActivity);
        switch (biometricManager.canAuthenticate()) {
            case BiometricManager.BIOMETRIC_SUCCESS:
                biometricCallback.onBiometricEnabled();
                break;
            case BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE:
                biometricCallback.onBiometricDisabled("No hay características biométricas disponibles en este dispositivo.");
                break;
            case BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE:
                biometricCallback.onBiometricDisabled("Las características biométricas no están disponibles actualmente");
                break;
            case BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED:
                biometricCallback.onBiometricDisabled("El usuario no ha asociado ninguna credencial biométrica con su cuenta.");
                break;
        }
    }

    public BiometricPrompt getBiometricPrompt(BiometricCallback biometricCallback){
        Executor executor;
        executor = ContextCompat.getMainExecutor(mAppcompactActivity);
        return new BiometricPrompt(mAppcompactActivity,
                executor, new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode, @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                biometricCallback.onAuthenticationError("Error al realizar la autentificación");
            }

            @Override
            public void onAuthenticationSucceeded(@NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                biometricCallback.onAuthenticationSucceeded();
            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
                biometricCallback.onAuthenticationFailed("el dedo se movió demasiado rápido !");

            }
        });
    }



}
